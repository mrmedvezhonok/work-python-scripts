def open_fin(fn, encoding='utf-8'):
    return open(fn, 'r', encoding=encoding)


def open_fout(fn, encoding='utf-8'):
    return open(fn, 'w', encoding=encoding)