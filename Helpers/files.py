def open_fin(fn, encoding='utf-8'):
    fin = open(fn, 'r', encoding=encoding)
    return fin


def open_fout(fn, encoding='utf-8'):
    fout = open(fn, 'w', encoding=encoding)
    return fout


def ofin(fn, encoding='utf-8'):
    fin = open_fin(fn, encoding)
    return fin


def ofout(fn, encoding='utf-8'):
    fout = open_fout(fn, encoding)
    return fout


def read_fin(fn, encoding='utf-8'):
    with open_fin(fn, encoding) as fin:
        source = fin.read()
    return source


def output_fout(output_text, fn, encoding='utf-8'):
    with open_fout(fn, encoding) as fout:
        fout.write(output_text)
        fout.flush()
