import os


def main():
    path = r'C:\Users\Hitrin_s\StudioProjects\mobios\lib\src'
    code_ext = ['dart']

    files_count = sum_full = sum_no_service = max_number = max_number_no_service = comments_count = 0
    max_fn = max_fn_no_service = ''

    for root, dirs, files in os.walk(path):
        files_count += len(files)
        for fn in files:
            ext = fn.split('.')[-1]
            if ext in code_ext:
                with open(os.path.join(root, fn), 'r', encoding='utf-8') as fin:
                    lines = fin.read().splitlines()
                comments_in_file = len(list(filter(lambda x: x.strip().startswith('//'), lines)))
                # comments_in_file = 0
                comments_count += comments_in_file
                number = len(lines) - comments_in_file
                sum_full += number
                if number > max_number:
                    max_number = number
                    max_fn = fn
                print(fn, number)

                if 'service' not in fn:
                    sum_no_service += number - comments_in_file
                    if number > max_number_no_service:
                        max_number_no_service = number
                        max_fn_no_service = fn

    print('\n-----_____-----_____-----_____-----')
    print('Всего файлов:', files_count, '\n')
    print(f'Всего строк: {sum_full}. Самый большой файл - {max_fn} ({max_number})')
    print('\nБез учёта сервисов:')
    print(f'Всего строк: {sum_no_service}. Самый большой файл - {max_fn_no_service} ({max_number_no_service})')
    print(f'Комментариев в коде: {comments_count}')
    print('-----_____-----_____-----_____-----')


if __name__ == '__main__':
    main()
