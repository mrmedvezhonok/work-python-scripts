#Pdf_copied_text_processing

from Helpers import *
import re


def processing_line(x):
    if x.endswith('-\n'):
        x = x[:-2]

    x = x.replace('\n', ' ')
    x = x.replace('*line_end*', '\n')
    x = x.replace(' ', ' ').replace('  ', ' ') # tabs and doble spaces to once spaces
    # if x.endswith('; ') or x.endswith(': '):
    #     x = x.replace(x[-1], x[-1] + '\n')
    if '[' in x:
        x = re.sub(r'\[[\d,\.\s!?]*\]', '', x)
    return x


if __name__ == '__main__':
    source_fn = 'pdf_copied_text_source.txt'

    with open_fin(source_fn) as fin:
        text = fin.read()

    lines = text.splitlines()
    # мне нужно оставить знаки переноса строки
    lines = list(map(lambda x: x + '\n', lines))
    lines = list(map(lambda x: processing_line(x), lines))

    with open_fout('output.txt') as fout:
        fout.writelines(lines)
        fout.flush()

    print('done')