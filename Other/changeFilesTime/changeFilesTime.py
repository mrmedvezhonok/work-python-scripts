import os

import datetime
import pytz
import pywintypes
import win32con
import win32file


def changeFileCreationTime(fname, newtime):
    wintime = pywintypes.Time(newtime)
    winfile = win32file.CreateFile(fname, win32con.GENERIC_WRITE,
                                   win32con.FILE_SHARE_READ |
                                   win32con.FILE_SHARE_WRITE |
                                   win32con.FILE_SHARE_DELETE,
                                   None,
                                   win32con.OPEN_EXISTING,
                                   win32con.FILE_ATTRIBUTE_NORMAL,
                                   None)

    win32file.SetFileTime(winfile, wintime, wintime, wintime)
    # None doesnt change args = file,     creation, last access, last write
    # win32file.SetFileTime(None, None, None, None) # does nonething
    winfile.close()


if __name__ == "__main__":
    path = r'C:\Users\Hitrin_s\PycharmProjects\CommonWorkProject\magistrate\MeanShift implementation'
    local_tz = pytz.timezone('Asia/Almaty')
    start_date = local_tz.localize(datetime.datetime(2021, 2, 7, 19, 29, 41), is_dst=None)

    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            print(os.path.join(root, name))
            changeFileCreationTime(os.path.join(root, name), start_date)
