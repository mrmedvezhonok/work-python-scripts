from moviepy.editor import VideoFileClip


if __name__ == '__main__':
    file_path = input('Путь до видео файла: ')
    input_video = VideoFileClip(file_path).rotate(90)
    output_video = input_video.without_audio()

    output_path = input('Как сохранить? (Путь и имя файла): ')
    output_video.write_videofile(output_path)
