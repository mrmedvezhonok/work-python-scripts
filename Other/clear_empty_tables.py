
if __name__ == '__main__':
    with open('list_to_drop.txt', 'r', encoding='utf-8') as fin:
        source = fin.read()

    lines = source.splitlines()
    lines = list(filter(lambda x: 'App' not in x, lines))
    print('lines count: ', len(lines))

    f = open('clear_tables_0.sql', 'w', encoding='utf-8-sig')

    q = 'DROP TABLE IF EXISTS dbo.[{}];'

    for l in lines:
        l = l.strip()
        # print(q.format(l))
        f.write(q.format(l))
        f.write('\n')

    f.close()
