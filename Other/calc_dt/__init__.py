from datetime import datetime as dt
from helpers import *
import json

if __name__ == '__main__':
    fn1 = 'source_workPositions_толебаева.json'
    fn2 = 'source_хитрин.json'

    with ofin(fn1) as fin:
        data = json.load(fin)
    dt_pairs = list()

# вытаскиваем нужные даты и складываем в пары (начало, конец)
    for row in data:
        if row['PositionType'] == 1:
            sdate_str = row['StartDate'][:10]
            sd, sm, sy = tuple(map(int, sdate_str.split('.')))
            start_date = dt(sy, sm, sd)

            if row['FinishDate']:
                edate_str = row['FinishDate'][:10]
                ed, em, ey = tuple(map(int, edate_str.split('.')))
                end_date = dt(ey, em, ed)
            else:
                end_date = dt.today()

            pair = (start_date, end_date)
            dt_pairs.append(pair)

    print(dt_pairs)
    exp_days = 0

    for start, end in dt_pairs:
        delta = end - start
        exp_days += delta.days
        print(f'Pair: {start} - {end}. Days {delta.days}')

    days_per_year = 365.25
    years = exp_days // days_per_year
    months = int((exp_days/days_per_year - years) * 12)
    print(f'{years} лет, {months} месяцев. Суммарно дней работы: {exp_days}')
