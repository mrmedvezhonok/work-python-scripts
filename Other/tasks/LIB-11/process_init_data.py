import json

fn1 = 'kz-en.txt'
fn2 = 'xavkina.txt'

fn_out = 'result_indent.json'

def dump_init_data():

    f1 = open(fn1, 'r', encoding='utf-8')
    f2 = open(fn2, 'r', encoding='utf-8')
    f_out = open(fn_out, 'w', encoding='utf-8')

    data = {}

    # читаем первый файл
    # здесь первый столбец - AuthorName
    # второй - Name (Index)
    lines = f1.readlines()
    for line in lines:
        line = line.strip()
        if not line: continue
        author, name = line.split()
        data[name] = author

    # Читаем второй файл, там столбцы наоборот идут
    lines = f2.readlines()
    for line in lines:
        line = line.strip()
        if not line: continue
        values = line.split()
        values2 = line.split('\t')
        name, author = line.split('\t')
        data[name] = author

    json.dump(data, f_out, ensure_ascii=False, indent=True)


def main():
    print('main')
    dump_init_data()

if __name__ == '__main__':
    main()
