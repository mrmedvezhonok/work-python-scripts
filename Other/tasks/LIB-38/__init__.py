import json
from bs4 import BeautifulSoup

url = "https://research.itmo.ru/ru/stat/106/{page}/spisok_kodov_grnti.htm"

PAGE_COUNT = 259


def get_page_content(page):
    import requests
    response = requests.get(url.format(page=page))
    print(f'request. page: {page}, status: {response.status_code}, encoding: {response.encoding}, elapsed: {response.elapsed}, ok: {response.ok}')
    return response.text


def parse_rows(content):
    from bs4 import NavigableString

    # парсер html
    soup = BeautifulSoup(content, 'html.parser')
    soup.find('table')
    table = soup.find('table')
    data = []
    for child in table.children:
        if isinstance(child, NavigableString): continue
        vals = child.text.split('\n')
        code, name = vals[1], vals[2]
        data.append((code, name))
    data.remove(data[0])
    return data


def main():
    print('hi')
    rows = []

    # Загрузка страницы с данными
    for current_page in range(1, PAGE_COUNT + 1):

        content = get_page_content(current_page)

        # запись данных (не нужна, добавил для отладки)
        with open(f'pages/response_page{current_page}.html', 'w', encoding='utf-8') as f:
            f.write(content)

        data = parse_rows(content)
        rows.extend(data)

    with open('result.json', 'w', encoding='utf-8') as fout:
        json.dump(rows, fout, ensure_ascii=False, indent=2)


if __name__ == '__main__':
    main()
