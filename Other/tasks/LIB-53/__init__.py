from out_queries import *

import json


def main():

    # source_fn = 'out.html'
    # # source_fn = 'LIB-53/out.html'
    #
    # with open(source_fn, 'r', encoding='ansi') as fin:
    #     content = fin.read()

    table_name = 'Ref_LBC'
    source_fn = 'data/LIB-53_dataPairs.json'
    # source_fn = 'LIB-53/data/LIB-53_dataPairs.json'

    with open(source_fn, 'r', encoding='utf-8') as fin:
        data = json.load(fin)

    for row in data:
        if "'" in row['code']:
            code = row['code'].replace("'", "''")
            row['code'] = code

    out_name_code_pairs(data, table_name)


if __name__ == '__main__':
    main()
