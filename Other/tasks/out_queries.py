import json
from os.path import join

script_sql_server_options = '''SET DATEFORMAT ymd
SET ARITHABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS, XACT_ABORT OFF\n\n'''


def normalize_str_to_russian_only(source):
    import string
    result = source.strip()
    if not string.ascii_letters in source: return result

    signs_one_register = {'u': 'и', 'H': 'Н', 'b': 'ь', 'n': 'п', 'B': 'В', }
    signs_two_registers = {'a': 'а', 'e': 'е', 'y': 'у', 'o': 'о', 'p': 'р', 'k': 'к', 'x': 'х', 'm': 'м', }

    if not result:
        return ''

    for sign in signs_one_register:
        result = result.replace(sign, signs_one_register[sign])
    for sign in signs_two_registers:
        result = result.replace(sign.lower(), signs_two_registers[sign].lower())
        result = result.replace(sign.upper(), signs_two_registers[sign].upper())

    return result


def get_filtered_list(source):
    return list(map(normalize_str_to_russian_only, filter(bool, source.splitlines())))


def open_file(fn=None, table_name='NoneTable', fout_path=r'C:\Users\Hitrin_s\PycharmProjects\CommonWorkProject\sql_out',
              encoding='utf-8-sig'):
    fn = f'{table_name}.sql' if fn is None else fn
    return open(join(fout_path, fn), 'w', encoding=encoding)


def out_name_code(spliter, source, table_name, with_print=None, fout_path=None, f=None):
    q1 = f"INSERT dbo.{table_name}(Gid, IsBlocked, Code, NameL1, NameL2, NameL3, NameL4, NameL5)"
    q2 = "VALUES (NEWID(), CONVERT(bit, 'False'), N'{code}', N'{name}', N'{name}', N'{name}', NULL, NULL)"
    q = f"{q1} {q2}"

    if isinstance(source, str):
        lines = get_filtered_list(source)
    else:
        lines = source

    if fout_path is not None:
        f = open_file(table_name=table_name, fout_path=fout_path) if f is None else f
    else:
        f = open_file(table_name=table_name) if f is None else f
    f.write(script_sql_server_options)

    if with_print is None:
        with_print = True if len(lines) <= 10 else False

    for line in lines:
        sp = spliter(line)
        name, code = sp

        query = q.format(code=code, name=name)
        f.write(query)
        f.write('\n')
        if with_print:
            print(query)
    f.flush()
    f.close()
    print('out_name_code done')


def out_name_code_multistring(spliter, source, table_name, with_print=None, fout_path=None, f=None):
    q1 = f"INSERT dbo.{table_name}(Gid, IsBlocked, Code, NameL1, NameL2, NameL3, NameL4, NameL5)"
    q2 = "VALUES (NEWID(), CONVERT(bit, 'False'), N'{code}', N'{name_kaz}', N'{name_rus}', N'{name_eng}', NULL, NULL)"
    q = f"{q1} {q2}"

    if isinstance(source, str):
        lines = get_filtered_list(source)
    else:
        lines = source

    if fout_path is not None:
        f = open_file(table_name=table_name, fout_path=fout_path) if f is None else f
    else:
        f = open_file(table_name=table_name) if f is None else f
    f.write(script_sql_server_options)

    if with_print is None:
        with_print = True if len(lines) <= 10 else False

    for line in lines:
        name_kaz, name_rus, name_eng, code = spliter(line)

        query = q.format(code=code, name_kaz=name_kaz, name_rus=name_rus, name_eng=name_eng)
        f.write(query)
        f.write('\n')
        if with_print:
            print(query)
    f.flush()
    f.close()
    print('out_name_code done')


def out_name(source, table_name, isMultiString=True, with_print=None, fout_path=None, f=None):
    if isMultiString:
        q1 = f"INSERT dbo.{table_name}(Gid, IsBlocked, NameL1, NameL2, NameL3, NameL4, NameL5)"
        q2 = "VALUES (NEWID(), CONVERT(bit, 'False'), N'{name}', N'{name}', N'{name}', NULL, NULL)"
        q = f"{q1} {q2}"
    else:
        q1 = f"INSERT dbo.{table_name}(Gid, IsBlocked, Name)"
        q2 = "VALUES (NEWID(), CONVERT(bit, 'False'), N'{name}')"
        q = f"{q1} {q2}"

    if isinstance(source, str):
        lines = get_filtered_list(source)
    else:
        lines = source

    if fout_path is not None:
        f = open_file(table_name=table_name, fout_path=fout_path) if f is None else f
    else:
        f = open_file(table_name=table_name) if f is None else f
    f.write(script_sql_server_options)

    if with_print is None:
        with_print = True if len(lines) <= 10 else False

    for line in lines:
        name = line
        query = q.format(name=name)
        if with_print:
            print(query)
        f.write(query)
        f.write('\n')
    f.flush()
    f.close()
    print('out_name done')


def lib61_with_code():
    table_name = 'Ref_ServiceMarks'
    if not table_name:
        table_name = input('table_name: ')

    source_path = r'C:\Users\Hitrin_s\PycharmProjects\CommonWorkProject\data'
    source_fn = 'LIB-61_dataPairs.json'

    with open(join(source_path, source_fn), 'r', encoding='utf-8') as fin:
        # source = fin.read()
        source = json.load(fin)

    out_name_code(lambda x: (x['name'], x['code']), source, table_name, True)


def lib61():
    table_name = 'Ref_ServiceMarks'
    if not table_name:
        table_name = input('table_name: ')

    source_path = r'C:\Users\Hitrin_s\PycharmProjects\CommonWorkProject\data'
    source_fn = 'LIB-61_dataPairs.json'

    with open(join(source_path, source_fn), 'r', encoding='utf-8') as fin:
        # source = fin.read()
        source = json.load(fin)

    lines = []
    for row in source:
        lines.append(row['name'])

    out_name(lines, table_name, True, True)


def lib_64():
    source = '''Абонемент
Хранение
Читальный зал'''
    table_name = 'Ref_SigleStorageInstances'
    if not table_name:
        table_name = input('table_name: ')

    out_name(source, table_name, True, True)


def lib_69():
    source_path = r'C:\Users\Hitrin_s\PycharmProjects\CommonWorkProject\data\69'
    source_fn = 'LIB-69_{0}_dataPairs.json'
    table_name = 'Ref_UDC'

    lines = []

    for i in range(1, 6):
        with open(join(source_path, source_fn.format(i)), 'r', encoding='utf-8') as fin:
            # source = fin.read()
            source = json.load(fin)
            lines.extend(source)

    out_name_code(lambda x: (x['name'], x['code']), lines, table_name, False)


def main():
    lib_69()


if __name__ == '__main__':
    main()
