
import xml.etree.ElementTree as ET


def get_tree(path='../data/', fn='JsStrings.resx'):
    # tree = ET.parse(f'{path}/{fn}')
    # root = tree.getroot()

    return ET.parse(f'{path}/{fn}')
#
#
# def get_res_pairs(path='../data/', fn='JsStrings.resx'):
#     root = get_tree(path, fn).getroot()
#
#     pairs = []
#     for child in root.findall('data'):
#         pairs.append((child.attrib.get('name'), child[0].text))
#     return pairs

def get_res_pairs(path='../data/', fn='JsStrings.resx'):
    root = get_tree(path, fn).getroot()

    pairs = {}
    for child in root.findall('data'):
        pairs[child.attrib.get('name')] = child[0].text
    return pairs


def add_subelement(parent, name, valueText):
    data = ET.SubElement(parent, 'data', {'name': name, 'xml:space':"preserve"})
    value = ET.SubElement(data, 'value')
    value.text = valueText
    return data, value


if __name__ == '__main__':
    pairs = get_res_pairs()
    print(pairs)
