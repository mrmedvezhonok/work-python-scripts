from js_strings import *
from resx_strings import *
import xml.etree.ElementTree as ET

if __name__ == '__main__':
    js_pairs = get_strings(read_data())
    print('js: ', js_pairs)

    resx_pairs = get_res_pairs()
    print('resx: ', resx_pairs)

    # js_keys = set()
    # for row in js_pairs:
    #     js_keys.add(row[0])
    #
    # resx_keys = set()
    # for row in resx_pairs:
    #     resx_keys.add(row[0])
    #
    # print('\n\n')

    js_diff = js_pairs.keys() - resx_pairs.keys()
    print('js_diff: ', len(js_diff), js_diff)

    resx_diff = resx_pairs.keys() - js_pairs.keys()
    print('resx_diff: ', len(resx_diff), resx_diff)

    tree = get_tree()
    resx_root = tree.getroot()

    for key in js_diff:
        print(f'key: {key}; value: {js_pairs[key]}')
        data, value = add_subelement(resx_root, key, js_pairs[key])
        print('added: ', data.attrib['name'], ' : ', value.text)

    tree.write('../data/out.resx')
