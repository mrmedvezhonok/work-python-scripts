# for line in lines:
#     line = line.strip()
#     if line.endswith('{'):
#         prev_block_name = block_name
#         block_name = line.split()[0]
#         if not ended_block:
#             block_name += '_'+prev_block_name
#         ended_block = False
#         blocks[block_name] = []
#         continue
#     if line == '}':
#         ended_block = True

import json
#
#
# def get_strings(data: dict):
#     if data is None:
#         raise Exception('No data for parsing')
#     pairs = []
#     for key in data:
#         prefix = f'{key}_'
#         pairs.extend(get_inner_rows(data[key], prefix=f'{key}_', indent='\t'))
#         # name, value = row[r].split(':')
#         # pairs.append((f'{prefix}{name.strip()}', value.strip()))
#     return pairs
#
#
# def get_inner_rows(data: dict, prefix='', indent='', highlight='_____=====_____'):
#     if not isinstance(data, dict):
#         print('not a dict: ', data)
#         return []
#
#     pairs = []
#     for key in data:
#         if isinstance(data[key], dict):
#             pairs.extend(get_inner_rows(data[key], prefix=f'{prefix}{key}_', indent=indent+'\t'))
#             continue
#         name, value = key, data[key]
#         pairs.append((f'{prefix}{name.strip()}', value.strip()))
#     return pairs


def get_strings(data: dict):
    if data is None:
        raise Exception('No data for parsing')
    pairs = {}
    for key in data:
        prefix = f'{key}_'
        pairs.update(get_inner_rows(data[key], prefix=f'{key}_', indent='\t'))
        # name, value = row[r].split(':')
        # pairs.append((f'{prefix}{name.strip()}', value.strip()))
    return pairs


def get_inner_rows(data: dict, prefix='', indent='', highlight='_____=====_____'):
    if not isinstance(data, dict):
        print('not a dict: ', data)
        return []

    pairs = {}
    for key in data:
        if isinstance(data[key], dict):
            pairs.update(get_inner_rows(data[key], prefix=f'{prefix}{key}_', indent=indent+'\t'))
            continue
        name, value = key, data[key]
        pairs[f'{prefix}{name.strip()}'] = value.strip()
    return pairs


def read_data(path='../data', fn='str.json'):
    from os.path import join
    with open(join(path, fn), 'r', encoding='utf-8') as fin:
        data = json.load(fin)
    return data


if __name__ == '__main__':
    with open('../data/str.json', 'r', encoding='utf-8') as fin:
        data = json.load(fin)

    pairs = get_strings(data)
    print(pairs)
    with open('strings.txt', 'w', encoding='utf-8') as fout:
        for pair in pairs:
            fout.write(f'{pair[0]}\t{pair[1]}\n')