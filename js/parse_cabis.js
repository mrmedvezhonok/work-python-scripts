task = '69_2'

tbl = document.querySelector('tbody');
rows = tbl.querySelectorAll('tr');

// Way to download data as fileCreatedDate
function download(content, fileName, contentType) {
    var a = document.createElement("a");
    var file = new Blob([content], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
};




// by code
// dataKeys = {};

// rows.forEach( row => {
	// tds = row.querySelectorAll('td');
	// name = tds[1].textContent.trim();
	// code = tds[2].textContent.trim();
	// dataKeys[code] = name;
// });

// dataKeysJson = JSON.stringify(dataKeys);
// download(dataKeysJson, 'LIB-'+task+'_dataKeys.json', 'text/plain');

//pairs
dataPairs = []
rows.forEach( row => {
	tds = row.querySelectorAll('td');
	name = tds[1].textContent.trim();
	code = tds[2].textContent.trim();
	dataPairs.push({code: code, name: name});
});

dataPairsJson = JSON.stringify(dataPairs);
download(dataPairsJson, 'LIB-'+task+'_dataPairs.json', 'text/plain');


