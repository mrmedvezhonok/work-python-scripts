import json
import os

from Helpers import open_fout


def main():
    fout = open_fout('output.txt')
    path = 'questions/'
    for fn in os.listdir(path):
        with open(os.path.join(path, fn)) as f:
            data = json.load(f)
            print(data['number'])
            fout.write(f"{data['number']}. {data['quetion']}\n{data['answer']}\n\n")
            fout.flush()
    fout.close()




if __name__ == '__main__':
    main()
