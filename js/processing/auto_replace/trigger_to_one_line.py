import os

if __name__ == '__main__':

    root_dir = r'C:\Users\Sergey\source\repos\Work\Library\Source\Library\Library.Web\Resources\js'
    template1 = '''triggers: {
									clear: {
										cls: 'x-form-clear-trigger',
										hidden: false,
										handler: function (field) {
											field.setValue('');
										}
									},
								}'''
    template2 = '''triggerClear: true'''

    for root, dirs, files in os.walk(root_dir):
        for filename in files:
            path = os.path.join(root, filename)
            with open(path, encoding='utf-8') as fin:
                text = fin.read()
            if template1 in text:
                print(path)
                output = text.replace(template1, template2)
                with open(path, 'w', encoding='utf-8') as fout:
                    fout.write(output)
