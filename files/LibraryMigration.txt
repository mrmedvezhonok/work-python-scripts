
Авторы : LIB-28
СДК - SDK : LIB-57

Migrations: 

Other: 
1 - Publications полнотекстовый поиск, настройка полей
4 - Установка Collation Kazakh для Ref_AuthorMark2Digits.AuthorName

Init:
2 - Ref_ArrangementTypes ( Типы расстановки ) : LIB-10
3 - Ref_PublicationCategories ( Каталог изданий ) : LIB-23
5 - Ref_AuthorMark2Digits ( Авторский знак ) : LIB-11
6 - Ref_ContentTypes ( Типы содержания ) : LIB-36
7 - Ref_MediaTypes ( Типы носителей ) : LIB-37
8 - Ref_CSCSTI ( ГРНТИ ) : LIB-38
9 - Ref_HAC ( ВАК ) : LIB-42
10 - Ref_DisposalReason ( Причины выбытия ) : LIB-41
11 - Ref_InventoryTypes ( Типы инвентаризации ) : ( LIB-44 )
12 - Ref_Keywords ( Ключевые слова ) : LIB-52
13 - Ref_LBC ( ББК ) : LIB-53
14 - Ref_PlacesOfPublications  ( места изданий ) : LIB-55
15 - Ref_Publishers ( Издательства ): LIB-56
16 - Ref_ServiceMarks ( Службные отметки ): LIB-61
17 - Ref_SigleStorageInstances ( Сигла хранения экземпляров ): LIB-64
18 - Ref_UDC ( УДК ) : LIB-69
19 - Поправка связи ReaderEmployee - Employee. устанавливает ограничение ON DELETE SET NULL, чтобы сотрудники удалялись без проблем
20 - Ref_AgeCategories (Мираскина)
21 - Изменение типа данных DateTime на DateTime2 в Ref_Authors для полей - годы жизни автора

