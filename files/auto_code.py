items = list(map(lambda x: x.split('.')[1].strip(), s.split(',')))
items
['Name', 'Dynasty', 'Title', 'Organization', 'Subdivision', 'Place', 'LiveDateFrom', 'LiveDateTo', 'IsCollective']
for i in items:
...     print(f', string previous{i}', end="")
...     
for i in items:
...     print(i)
...     
, string previousName, string previousDynasty, string previousTitle, string previousOrganization, string previousSubdivision, string previousPlace, string previousLiveDateFrom, string previousLiveDateTo, string previousIsCollectiveName

out3 = ""
for i in items:
...     l = 'string previous{i} = obj.{i};'
...     out3 += f'{l}\n'
...     print(l)
...     
string previous{i} = obj.{i};
string previous{i} = obj.{i};
string previous{i} = obj.{i};
string previous{i} = obj.{i};
string previous{i} = obj.{i};
string previous{i} = obj.{i};
string previous{i} = obj.{i};
string previous{i} = obj.{i};
string previous{i} = obj.{i};

c = "if (!string.IsNullOrEmpty(previous{0})) "
r = 'var {0}Data = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_{1} && x.Index == index && x.Value == previous{1});'
set_value = "{0}Data.Value = author.{1};"
for i in items:
...     print(c.format(i) + '{')
...     print('\t' + r.format(i[0].lower() + i[1:], i))
...     print('\t' + set_value.format(i[0].lower() + i[1:], i))
...     print("}")
...            
if (!string.IsNullOrEmpty(previousName)) {
	var nameData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_Name && x.Index == index && x.Value == previousName);
	nameData.Value = author.Name;
}
if (!string.IsNullOrEmpty(previousDynasty)) {
	var dynastyData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_Dynasty && x.Index == index && x.Value == previousDynasty);
	dynastyData.Value = author.Dynasty;
}
if (!string.IsNullOrEmpty(previousTitle)) {
	var titleData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_Title && x.Index == index && x.Value == previousTitle);
	titleData.Value = author.Title;
}
if (!string.IsNullOrEmpty(previousOrganization)) {
	var organizationData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_Organization && x.Index == index && x.Value == previousOrganization);
	organizationData.Value = author.Organization;
}
if (!string.IsNullOrEmpty(previousSubdivision)) {
	var subdivisionData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_Subdivision && x.Index == index && x.Value == previousSubdivision);
	subdivisionData.Value = author.Subdivision;
}
if (!string.IsNullOrEmpty(previousPlace)) {
	var placeData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_Place && x.Index == index && x.Value == previousPlace);
	placeData.Value = author.Place;
}
if (!string.IsNullOrEmpty(previousLiveDateFrom)) {
	var liveDateFromData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_LiveDateFrom && x.Index == index && x.Value == previousLiveDateFrom);
	liveDateFromData.Value = author.LiveDateFrom;
}
if (!string.IsNullOrEmpty(previousLiveDateTo)) {
	var liveDateToData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_LiveDateTo && x.Index == index && x.Value == previousLiveDateTo);
	liveDateToData.Value = author.LiveDateTo;
}
if (!string.IsNullOrEmpty(previousIsCollective)) {
	var isCollectiveData = Uow.Query<MPublicationData>().FirstOrDefault(x => x.Key == EPublicationDataKey.Author_IsCollective && x.Index == index && x.Value == previousIsCollective);
	isCollectiveData.Value = author.IsCollective;
}'