﻿SELECT ap.Oid AS PersonId, ap.FullName, ap.Iin, pgo.Oid AS PgoId, grp.Oid, grp.Name_L1 AS [Group], pgo.IsActual, org.Oid AS OrgId, org.ShortNameL1 AS Organization
  FROM dbo.PersonGroupOrganizations pgo
  JOIN dbo.App_People ap
  ON pgo.Person = ap.Oid
  JOIN dbo.GRef_Organizations org
  ON pgo.Organization = org.Oid
  JOIN dbo.Ref_Groups grp
  ON pgo.[Group] = grp.Oid

  WHERE pgo.Person = 1 OR pgo.Person = 2
  