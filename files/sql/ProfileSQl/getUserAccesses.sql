﻿SELECT ap.Oid AS PersonId, ap.FullName, u.Oid AS UserId, access.Oid AS AccessId, access.[View], access.Edit, access.Moderate, org.Oid AS OrgId, org.ShortNameL1 AS OrganizationName, groups.Oid AS GroupId, groups.Name_L1 AS GroupName
  --, ul.Oid AS LoginId, ul.Login,
  FROM dbo.App_People ap
  JOIN dbo.App_Auth_Users u ON u.Person = ap.Oid
--  JOIN dbo.Apёp_Auth_UserLogins ul ON u.Oid = ul.[User]
  JOIN dbo.Access access ON access.[User] = u.Oid
  JOIN dbo.GRef_Organizations org ON access.Organization = org.Oid
  JOIN dbo.Ref_Groups groups ON groups.Oid = access.[Group]
  WHERE ap.FullName LIKE '%Тилеубай%'