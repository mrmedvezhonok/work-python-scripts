﻿
DROP TABLE IF EXISTS dbo.TUP_ItemHours;
DROP TABLE IF EXISTS dbo.TUP_ItemValues_ByHalfYear


DROP TABLE IF EXISTS dbo.[TUP_Tup-to-ParallelStep];
DROP TABLE IF EXISTS dbo.[TUP_Tup-to-Organization];
DROP TABLE IF EXISTS dbo.[TUP_Tup-to-Components];



DROP TABLE IF EXISTS dbo.[Load-to-Allocation];
DROP TABLE IF EXISTS dbo.Alc_Files;
DROP TABLE IF EXISTS dbo.Alc_Items;
DROP TABLE IF EXISTS dbo.TUP_ItemValues;
DROP TABLE IF EXISTS dbo.Alc_Versions;
DROP TABLE IF EXISTS dbo.TUP_Versions;


DROP TABLE IF EXISTS dbo.TeacherMng_TeacherPositionsAdditionalTutors_Alc_StudyGroupsStudyGroups;
DROP TABLE IF EXISTS dbo.TeacherMng_TeacherPositionsAdditionalTutors_Alc_StudyGroupsStudyGroups;

  
DROP TABLE IF EXISTS dbo.TUP_Items_BySubjectGroup;

DROP TABLE IF EXISTS dbo.[TUP_TupItemBySubjectChoice-to-Subject];
DROP TABLE IF EXISTS dbo.TUP_Items_BySubjectChoice;
DROP TABLE IF EXISTS dbo.TUP_Items_BySubject;
DROP TABLE IF EXISTS dbo.TUP_Items;


DROP TABLE IF EXISTS dbo.Rup_ItemValues_ByParallel;
DROP TABLE IF EXISTS dbo.Rup_ItemValues_ByKlass;
DROP TABLE IF EXISTS dbo.Rup_ItemValues;

DROP TABLE IF EXISTS dbo.Load_MLoadItemValuesByAlterAlcGroup;
DROP TABLE IF EXISTS dbo.Load_LoadItemValuesByGroup;
DROP TABLE IF EXISTS dbo.Load_LoadItemValuesByKlass;
DROP TABLE IF EXISTS dbo.Load_LoadItemValues;
DROP TABLE IF EXISTS dbo.Load_LoadItems;
DROP TABLE IF EXISTS dbo.Rup_Items;
DROP TABLE IF EXISTS dbo.Rup_Components;
DROP TABLE IF EXISTS dbo.Rup_Steps;

DROP TABLE IF EXISTS dbo.Schd_ReplacementItem;
DROP TABLE IF EXISTS dbo.Schd_CancelLesson;
DROP TABLE IF EXISTS dbo.Schd_ScheduleItem;
DROP TABLE IF EXISTS dbo.Schd_Schedule;
DROP TABLE IF EXISTS dbo.Schd_LessonToLoad;
DROP TABLE IF EXISTS dbo.[Load-to-AlterAlcGroup];
DROP TABLE IF EXISTS dbo.Load_Files;

DROP TABLE IF EXISTS dbo.KlsDist;


DROP TABLE IF EXISTS dbo.KlsDist;