﻿SELECT TOP 100 t.Name AS Table_Name, idx.name AS Index_Name
  FROM sys.indexes idx
  JOIN sys.index_columns ic ON ic.object_id = idx.object_id AND ic.index_id = idx.index_id
  JOIN sys.tables t ON idx.object_id = t.object_id
  WHERE idx.name LIKE 'pk_docum%'
--  WHERE idx.name LIKE 'pk_groups'