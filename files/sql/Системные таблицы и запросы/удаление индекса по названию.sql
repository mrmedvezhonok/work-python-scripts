﻿BEGIN TRANSACTION;

DECLARE @ObjectsIdOfIndexesToDelete TABLE (obj_id INT, idx_id INT);

INSERT INTO @ObjectsIdOfIndexesToDelete(obj_id, idx_id)
  VALUES (
    SELECT idx.
    FROM sys.indexes idx
    JOIN sys.index_columns ic ON ic.object_id = idx.object_id AND ic.index_id = idx.index_id
    WHERE idx.name LIKE 'pk_Groups'
  )

SELECT *
  FROM sys.indexes idx
  JOIN sys.index_columns ic ON ic.object_id = idx.object_id AND ic.index_id = idx.index_id

  WHERE idx.name LIKE 'pk_groups'
  ;
  
COMMIT TRANSACTION;