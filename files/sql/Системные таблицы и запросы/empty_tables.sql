﻿SELECT * FROM
(
 SELECT 
  [TableName] = so.name, 
  [RowCount] = MAX(si.rows) 
 FROM 
  sysobjects so, 
  sysindexes si 
 WHERE 
  so.xtype = 'U' 
  AND 
  si.id = OBJECT_ID(so.name) 
 GROUP BY 
  so.name 
) sub
WHERE sub.[RowCount] = 0;

--  declare @name varchar(128), @sql nvarchar(2000), @i int
--select  @name = ''
--while   @name < (select max(name) from sysobjects where xtype = 'U')
--begin
--    select @name = min(name) from sysobjects where xtype = 'U' and name > @name
--    select @sql = 'select @i = count(*) from [' + @name + ']'
--    exec sp_executesql @sql, N'@i int out', @i out
--    if @i = 0
--    begin
--        select @sql = 'drop table [' + @name + ']'
--        print @sql
--            -- unmask next to drop the table
--        -- exec (@sql)
--    end
--end