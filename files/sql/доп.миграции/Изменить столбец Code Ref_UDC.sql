﻿-- Начать транзакцию
--
BEGIN TRANSACTION

--
-- Изменить столбец [Code] для таблицы [dbo].[Ref_UDC]
--
ALTER TABLE dbo.Ref_UDC
  ALTER
    COLUMN Code nvarchar(max);

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

--
-- Фиксировать транзакцию
--
IF @@TRANCOUNT>0 COMMIT TRANSACTION

--
-- Установить NOEXEC в состояние off
--
SET NOEXEC OFF;