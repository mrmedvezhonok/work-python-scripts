-- Миграции, которые нужно выполнить для бэкапа от 12.11.2020
-- Являются доп. потому что нет необходимости их в сборку добавлять. При инициализации новой БД всё без них будет как надо.
-- Чисто development process миграции.


-- Данные инициализации для Ref_LBC (ББК LIB-53) содержат повторяющиеся ключи, однако позднее, добавлен индекс и коды были отфильтрованы
TRUNCATE TABLE dbo.Ref_LBC;
--  ==============================!!!! ВЫПОЛНИТЬ МИГРАЦИЮ 13.sql !!!! ==============================

-- Удаляет ненужное поле из читателей
ALTER TABLE dbo.Readers
  DROP COLUMN IF EXISTS CardIssueDate

-- Изменить столбец [Code] для таблицы [dbo].[Ref_UDC]
--
ALTER TABLE dbo.Ref_UDC
  ALTER
    COLUMN Code nvarchar(100);
	

-- Удалить штрих код из экземпляров
ALTER TABLE dbo.Instances
  DROP COLUMN IF EXISTS Barcode

-- Исправляет создание связей для Instances

ALTER TABLE dbo.Instances
	DROP COLUMN IF EXISTS InventoryNumber
 
ALTER TABLE dbo.Instances
	DROP COLUMN IF EXISTS RegistrationCardNumber

ALTER TABLE dbo.Instances
  DROP COLUMN IF EXISTS RegistrationCardSubNumber
  
    
-- После обновления контингента (от 25.11) необходимо сбросить внешний ключ
DROP TABLE IF EXISTS dbo.Ctng_Doc_OrderOfCompleteTraining_TableParts;


