﻿-- Нет уровня 55cacfad-b004-45eb-9d0d-599e383810ca 1
-- Базовый уровень 23cac4f5-be04-4c12-8c0d-e64b5d5bba0e 2
-- Первый уровень 85d79cee-cb92-4f70-a646-56efb28c8f43 3
-- Учитель  610d0639-f73b-4225-9702-367f175d679b 4

DECLARE @sKILLs TABLE(Gid UNIQUEIDENTIFIER, Name NVARCHAR(MAX), [Index] BIGINT)
  INSERT INTO @sKILLs(Gid, Name, [Index]) VALUES('55cacfad-b004-45eb-9d0d-599e383810ca', N'Нет уровня', 1)
  INSERT INTO @sKILLs(Gid, Name, [Index]) VALUES('23cac4f5-be04-4c12-8c0d-e64b5d5bba0e', N'Базовый уровень', 2)
  INSERT INTO @sKILLs(Gid, Name, [Index]) VALUES('85d79cee-cb92-4f70-a646-56efb28c8f43', N'Первый уровень', 3)
  INSERT INTO @sKILLs(Gid, Name, [Index]) VALUES('610d0639-f73b-4225-9702-367f175d679b', N'Учитель', 4);

--SELECT *
--  FROM @sKILLs;

  SELECT *
    FROM @sKILLs row

    IF EXISTS (
        SELECT *
        FROM dbo.Ref_WorkSkillLevel
        WHERE Name_L1 = row.Name OR Name_L2 = row.Name
    )
    BEGIN
     UPDATE dbo.Ref_WorkSkillLevel
      SET Gid = row.Gid, [Index] = row.[Index]
      WHERE Name_L1 = row.Name OR Name_L2 = row.Name
    END;

    ELSE
      BEGIN
        INSERT INTO dbo.Ref_WorkSkillLevel(Gid, Name_L1, Name_L2, Name_L3, [Index])
          VALUES(row.Gid, row.Name, row.Name, row.Name, row.[Index])
        END;



