﻿USE profile_db
GO

INSERT INTO dbo.App_Auth_UserRoles
(
  Gid
 ,Disabled
 ,Role
 ,[User]
)
VALUES
(
  NEWID() -- Gid - uniqueidentifier
 ,0 -- Disabled - bit
 ,2 -- Role - bigint
 ,14253 -- User - bigint
);
GO