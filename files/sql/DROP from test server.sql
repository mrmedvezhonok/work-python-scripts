﻿USE library
GO

DROP TABLE IF EXISTS dbo.InventoryInstances
GO

DROP TABLE IF EXISTS dbo.Instances
GO

DROP TABLE IF EXISTS dbo.PublicationsData
GO

DROP TABLE IF EXISTS dbo.Publications
GO

DROP TABLE IF EXISTS dbo.Ref_Authors
GO

DROP TABLE IF EXISTS dbo.Ref_AuthorMark2Digits
GO

DROP TABLE IF EXISTS dbo.Ref_ArrangementTypes
GO

DROP TABLE IF EXISTS dbo.Ref_PublicationCategories
GO


DELETE FROM dbo.__ScriptHistory
WHERE FileId
GO
