﻿SELECT
  App_People.Oid AS PersonId
 ,App_People.Iin
 ,App_People.FullName
 ,Employees.Oid AS EmployeeId
 ,Employees.Position
 ,GRef_Organizations.Oid AS OrganizationId
 ,GRef_Organizations.NameL2 AS Organization
FROM dbo.Employees
INNER JOIN dbo.App_People
  ON Employees.Person = App_People.Oid
INNER JOIN dbo.GRef_Organizations
  ON Employees.Organization = GRef_Organizations.Oid
