﻿USE SE_DB
GO

INSERT INTO dbo.App_Auth_Users
(
  Gid
 ,Person
 ,UserName
 ,Locked
 ,ChangePassword
 ,IsAd
 ,IsLocked
 ,IsNeedSignOut
 ,UserProviderNamespace
 ,CurrentLogin
)
VALUES
(
  NEWID() -- Gid - uniqueidentifier
 ,9467 -- Person - bigint
 ,N'mishka' -- UserName - nvarchar(100)
 ,0 -- Locked - bit
 ,0 -- ChangePassword - bit
 ,0 -- IsAd - bit
 ,0 -- IsLocked - bit
 ,0 -- IsNeedSignOut - bit
 ,N'' -- UserProviderNamespace - nvarchar(100)
 ,N'mishka' -- CurrentLogin - nvarchar(100)
);
GO
  SELECT * FROM dbo.App_Auth_Users
    WHERE UserName = 'mishka'
