import re
import uuid

source = '''
      MDairyMarkQuarter(1, <MDairyMarkLesson>[
        MDairyMarkLesson('Learning the language 1.1', [
          MDairyMarkCriteria('Listening', 3.5, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.0, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.5, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.5, 'Writing is important part of life.'),
        ]),
        MDairyMarkLesson('Learning the language 1.2', [
          MDairyMarkCriteria('Listening', 3.6, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.1, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.4, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.6, 'Writing is important part of life.'),
        ]),

      ], <MDairyMarkLesson>[
        MDairyMarkLesson('Practise 1.1', [
          MDairyMarkCriteria('Listening', 3.8, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.8, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.3, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.3, 'Writing is important part of life.'),
        ]),
        MDairyMarkLesson('Practise 1.2', [
          MDairyMarkCriteria('Listening', 3.3, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.2, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.5, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.3, 'Writing is important part of life.'),
        ]),

      ]),
      MDairyMarkQuarter(2, <MDairyMarkLesson>[
        MDairyMarkLesson('Learning the language 2.1', [
          MDairyMarkCriteria('Listening', 3.5, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.0, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.5, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.5, 'Writing is important part of life.'),
        ]),
        MDairyMarkLesson('Learning the language 2.2', [
          MDairyMarkCriteria('Listening', 3.6, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.1, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.4, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.6, 'Writing is important part of life.'),
        ]),

      ], <MDairyMarkLesson>[
        MDairyMarkLesson('Practise 2.1', [
          MDairyMarkCriteria('Listening', 3.8, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.8, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.3, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.3, 'Writing is important part of life.'),
        ]),
        MDairyMarkLesson('Practise 2.2', [
          MDairyMarkCriteria('Listening', 3.3, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.2, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.5, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.3, 'Writing is important part of life.'),
        ]),

      ]),
      MDairyMarkQuarter(3, <MDairyMarkLesson>[
        MDairyMarkLesson('Learning the language 3.1', [
          MDairyMarkCriteria('Listening', 3.5, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.0, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.5, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.5, 'Writing is important part of life.'),
        ]),
        MDairyMarkLesson('Learning the language 3.2', [
          MDairyMarkCriteria('Listening', 3.6, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.1, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.4, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.6, 'Writing is important part of life.'),
        ]),

      ], <MDairyMarkLesson>[
        MDairyMarkLesson('Practise 3.1', [
            MDairyMarkCriteria('Listening', 3.8, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.8, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.3, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.3, 'Writing is important part of life.'),
        ]),
        MDairyMarkLesson('Practise 3.2', [
          MDairyMarkCriteria('Listening', 3.3, 'Listening is important part of life.'),
          MDairyMarkCriteria('Speaking', 4.2, 'Speaking is important part of life.'),
          MDairyMarkCriteria('Reading', 4.5, 'Reading is important part of life.'),
          MDairyMarkCriteria('Writing', 4.3, 'Writing is important part of life.'),
        ]),
      ]),'''


if __name__ == '__main__':

    lines = source.splitlines()

    fout = open('c:/Users/Hitrin_s/Documents/out.txt', 'w', encoding='utf-8')

    for line in lines:
        if not re.search(r'MDairyMarkCriteria', line):
            fout.write(line)
            fout.write('\n')
            fout.flush()
            continue

        output = line.replace('MDairyMarkCriteria(', f"MDairyMarkCriteria('{uuid.uuid4()}', ")
        fout.write(output)
        fout.write('\n')
        fout.flush()

    fout.close()