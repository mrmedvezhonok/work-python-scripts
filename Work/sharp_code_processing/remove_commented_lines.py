from Helpers import *


if __name__ == '__main__':
    source = read_fin('source_RCL1.txt')
    lines = source.splitlines()
    fout = ofout('output_RCL1.dart')
    for line in lines:
        if not '//' in line:
            fout.write(line)
            fout.write('\n')
            fout.flush()
    fout.close()
