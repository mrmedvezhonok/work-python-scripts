/// <summary> Ссылка на автора </summary>
		[FwCultureLocalize("EPublicationDataKey_Author", typeof(Strings))]
		Author = 1,
		/// <summary> название автора </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_Name", typeof(Strings))]
		Author_Name = 2,
		/// <summary> династия автора </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_dynasty", typeof(Strings))]
		Author_Dynasty = 3,
		/// <summary> титул автора </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_Title", typeof(Strings))]
		Author_Title = 4,
		/// <summary> организация автора </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_Organization", typeof(Strings))]
		Author_Organization = 5,
		/// <summary> годы жизни автора </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_LiveDates", typeof(Strings))]
		Author_LiveDates = 6,
		/// <summary> коллективный автор </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_IsCollective", typeof(Strings))]
		Author_IsCollective = 7,
		/// <summary> подразделение автора </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_Subdivision", typeof(Strings))]
		Author_Subdivision = 8,
		/// <summary> место автора </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_Place", typeof(Strings))]
		Author_Place = 9,
		/// <summary> авторский знак </summary>
		[FwCultureLocalize("EPublicationDataKey_Author_Mark", typeof(Strings))]
		Author_Mark = 10,
		/// <summary> заглавие </summary>
		[FwCultureLocalize("EPublicationDataKey_Title", typeof(Strings))]
		Title = 11,
		/// <summary> Роль автора (редактор, художник) </summary>
		[FwCultureLocalize("EPublicationDataKey_AuthorRole", typeof(Strings))]
		AuthorRole = 12,
		/// <summary> Кол-во незначащих символов в начале заглавия </summary>
		[FwCultureLocalize("EPublicationDataKey_InsignificantCharacters", typeof(Strings))]
		InsignificantCharacters = 13,
		/// <summary> тип носителя </summary>
		[FwCultureLocalize("EPublicationDataKey_MediaType", typeof(Strings))]
		MediaType = 14,
		/// <summary> параллельное заглавие (например, перевод заглавия) </summary>
		[FwCultureLocalize("EPublicationDataKey_ParallelTitle", typeof(Strings))]
		ParallelTitle = 15,
		/// <summary> язык параллельного заглавия </summary>
		[FwCultureLocalize("EPublicationDataKey_ParallelTitleLanguage", typeof(Strings))]
		ParallelTitleLanguage = 16,
		/// <summary> номер части тома </summary>
		[FwCultureLocalize("EPublicationDataKey_VolumeNumber", typeof(Strings))]
		VolumeNumber = 17,
		/// <summary> название части тома </summary>
		[FwCultureLocalize("EPublicationDataKey_VolumeName", typeof(Strings))]
		VolumeName = 18,
		/// <summary> Свед. Отн. К заглавию (Примеры: учебное пособие, справочник, повесть, в 3 томах, пер. с анг) </summary>
		[FwCultureLocalize("EPublicationDataKey_TitleInformation", typeof(Strings))]
		TitleInformation = 19,
		/// <summary> Свед. Об ответственности (Под ред. О.И, Рапп, РАН Ин-т философии) </summary>
		[FwCultureLocalize("EPublicationDataKey_ResponsibilityInformation", typeof(Strings))]
		ResponsibilityInformation = 20,
		/// <summary> Свед. об издании (Данные о переиздании, перепечатки и т.п.. Примеры: 2-е издание, Переиздание) </summary>
		[FwCultureLocalize("EPublicationDataKey_EditionInformation", typeof(Strings))]
		EditionInformation = 21,
		/// <summary> Свед. Об ответ. Изд. (Пример: предисл. К перееизд. А.А.Алиева) </summary>
		[FwCultureLocalize("EPublicationDataKey_EditionResponsibilityInformation", typeof(Strings))]
		EditionResponsibilityInformation = 22,
		/// <summary> Область нумерации и даты (Содержит нумерацию и\или даты первого и последнего выпуска сериального издания) </summary>
		[FwCultureLocalize("EPublicationDataKey_NumberingArea", typeof(Strings))]
		NumberingArea = 23,
		/// <summary> Вид и объем элект. Ресурса (Примеры: Электронный журнал, электранная прогр, текстовые данные) </summary>
		[FwCultureLocalize("EPublicationDataKey_TypeAndVolumeElectronicResource", typeof(Strings))]
		TypeAndVolumeElectronicResource = 24,
		/// <summary> место издания </summary>
		[FwCultureLocalize("EPublicationDataKey_PlaceOfPublication", typeof(Strings))]
		PlaceOfPublication = 25,
		/// <summary> издательство </summary>
		[FwCultureLocalize("EPublicationDataKey_Publisher", typeof(Strings))]
		Publisher = 26,
		/// <summary> год издания </summary>
		[FwCultureLocalize("EPublicationDataKey_PublishYear", typeof(Strings))]
		PublishYear = 27,
		/// <summary> специфичные сведения (Для НТ документов и закон. Актов (дисс, отчет, патент, стандарт, закон) </summary>
		[FwCultureLocalize("EPublicationDataKey_SpecificInformation", typeof(Strings))]
		SpecificInformation = 28,
		/// <summary> Тип специфич сведений (в замен, введен) </summary>
		[FwCultureLocalize("EPublicationDataKey_SpecificInformationType", typeof(Strings))]
		SpecificInformationType = 29,
		/// <summary> объем (кол-во страниц, листов, дисков) </summary>
		[FwCultureLocalize("EPublicationDataKey_Capacity", typeof(Strings))]
		Capacity = 30,
		/// <summary> другие физические характеристики </summary>
		[FwCultureLocalize("EPublicationDataKey_CapacityOther", typeof(Strings))]
		CapacityOther = 31,
		/// <summary> размеры </summary>
		[FwCultureLocalize("EPublicationDataKey_Size", typeof(Strings))]
		Size = 32,
		/// <summary> сопроводительные материалы </summary>
		[FwCultureLocalize("EPublicationDataKey_SupportingMaterials", typeof(Strings))]
		SupportingMaterials = 33,
		/// <summary> заглавие серии </summary>
		[FwCultureLocalize("EPublicationDataKey_SeriesTitle", typeof(Strings))]
		SeriesTitle = 34,
		/// <summary> сведения, относящ к серии (в 5 томах, сбор тезисов конф.) </summary>
		[FwCultureLocalize("EPublicationDataKey_SeriesInformation", typeof(Strings))]
		SeriesInformation = 35,
		/// <summary> сведения, относящ об отв. серии (редактор, главный редактор) </summary>
		[FwCultureLocalize("EPublicationDataKey_SeriesResponsibilityInformation", typeof(Strings))]
		SeriesResponsibilityInformation = 36,
		/// <summary> ISSN серии </summary>
		[FwCultureLocalize("EPublicationDataKey_SeriesISSN", typeof(Strings))]
		SeriesISSN = 37,
		/// <summary> подсерия </summary>
		[FwCultureLocalize("EPublicationDataKey_Subseries", typeof(Strings))]
		Subseries = 38,
		/// <summary> ISSN подсерии </summary>
		[FwCultureLocalize("EPublicationDataKey_SubseriesISSN", typeof(Strings))]
		SubseriesISSN = 39,
		/// <summary> номер выпуска, тома серии </summary>
		[FwCultureLocalize("EPublicationDataKey_SeriesVolumeNumber", typeof(Strings))]
		SeriesVolumeNumber = 40,
		/// <summary> заглавие источника (Комсомольская правда, техника) </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceTitle", typeof(Strings))]
		SourceTitle = 41,
		/// <summary> сведения, относящ к источнику (Материал 5-й респ научн конф ) </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceInformation", typeof(Strings))]
		SourceInformation = 42,
		/// <summary> Свед об отв источника (КазГУ, Мин-во образования) </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceResponsibilityInformation", typeof(Strings))]
		SourceResponsibilityInformation = 43,
		/// <summary> авторы источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceAuthors", typeof(Strings))]
		SourceAuthors = 44,
		/// <summary> Сведения об изд источника (2-е издание) </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceEditionInformation", typeof(Strings))]
		SourceEditionInformation = 45,
		/// <summary> место издания источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourcePlaceOfPublication", typeof(Strings))]
		SourcePlaceOfPublication = 46,
		/// <summary> издательство источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourcePublisher", typeof(Strings))]
		SourcePublisher = 47,
		/// <summary> год издания источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourcePublishYear", typeof(Strings))]
		SourcePublishYear = 48,
		/// <summary> физические хар-ки источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceCapacityOther", typeof(Strings))]
		SourceCapacityOther = 49,
		/// <summary> номер части, тома источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceVolumeNumber", typeof(Strings))]
		SourceVolumeNumber = 50,
		/// <summary> Страницы источника, на которых помещена составная часть </summary>
		[FwCultureLocalize("EPublicationDataKey_SourcePages", typeof(Strings))]
		SourcePages = 51,
		/// <summary> параллельное заглавие источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceParallelTitle", typeof(Strings))]
		SourceParallelTitle = 52,
		/// <summary> серия источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceSeries", typeof(Strings))]
		SourceSeries = 53,
		/// <summary> ISSN источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceISSN", typeof(Strings))]
		SourceISSN = 54,
		/// <summary> ISBN источника </summary>
		[FwCultureLocalize("EPublicationDataKey_SourceISBN", typeof(Strings))]
		SourceISBN = 55,
		/// <summary> электронный адрес ресурса </summary>
		[FwCultureLocalize("EPublicationDataKey_Site", typeof(Strings))]
		Site = 56,
		/// <summary> примечания к электронному адресу </summary>
		[FwCultureLocalize("EPublicationDataKey_SiteNotes", typeof(Strings))]
		SiteNotes = 57,
		/// <summary> системные требования </summary>
		[FwCultureLocalize("EPublicationDataKey_SystemRequirements", typeof(Strings))]
		SystemRequirements = 58,
		/// <summary> общие примечания </summary>
		[FwCultureLocalize("EPublicationDataKey_Notes", typeof(Strings))]
		Notes = 59,
		/// <summary> Примечания о содержании (Иванов И,И Батька: Рассказ С 3-23) </summary>
		[FwCultureLocalize("EPublicationDataKey_NotesContent", typeof(Strings))]
		NotesContent = 60,
		/// <summary> библиография </summary>
		[FwCultureLocalize("EPublicationDataKey_Bibliography", typeof(Strings))]
		Bibliography = 61,
		/// <summary> аннотация (реферат) </summary>
		[FwCultureLocalize("EPublicationDataKey_Annotation", typeof(Strings))]
		Annotation = 62,
		/// <summary> номер гос. регистрации </summary>
		[FwCultureLocalize("EPublicationDataKey_StateRegistrationNumber", typeof(Strings))]
		StateRegistrationNumber = 63,
		/// <summary> ISSN </summary>
		[FwCultureLocalize("EPublicationDataKey_ISSN", typeof(Strings))]
		ISSN = 64,
		/// <summary> ISBN </summary>
		[FwCultureLocalize("EPublicationDataKey_ISBN", typeof(Strings))]
		ISBN = 65,
		/// <summary> цена </summary>
		[FwCultureLocalize("EPublicationDataKey_Price", typeof(Strings))]
		Price = 66,
		/// <summary> (тираж 3000 экзмепляров) </summary>
		[FwCultureLocalize("EPublicationDataKey_Circulation", typeof(Strings))]
		Circulation = 67,
		/// <summary> спецификация тома (Содержит перечень номеров, томов и др печатных единиц сер изд ) </summary>
		[FwCultureLocalize("EPublicationDataKey_VolumeSpecification", typeof(Strings))]
		VolumeSpecification = 68,
		/// <summary> ключевое слово </summary>
		[FwCultureLocalize("EPublicationDataKey_Keyword", typeof(Strings))]
		Keyword = 70,
		/// <summary> УДК </summary>
		[FwCultureLocalize("EPublicationDataKey_UDC", typeof(Strings))]
		UDC = 71,
		/// <summary> Предметная рубрика </summary>
		[FwCultureLocalize("EPublicationDataKey_LBC", typeof(Strings))]
		LBC = 101,
		/// <summary> ББК</summary>
		[FwCultureLocalize("EPublicationDataKey_LBC_Code", typeof(Strings))]
		LBC_Code = 102,
		/// <summary> ВАК </summary>
		[FwCultureLocalize("EPublicationDataKey_HAC", typeof(Strings))]
		HAC = 73,
		/// <summary> ГРНТИ </summary>
		[FwCultureLocalize("EPublicationDataKey_CSCSTI", typeof(Strings))]
		CSCSTI = 74,
		/// <summary> географическое название </summary>
		[FwCultureLocalize("EPublicationDataKey_GeographicName", typeof(Strings))]
		GeographicName = 75,
		/// <summary> Формат (Специальный код, который присваивается каждому изданию для хранения его при форматной расстановке. Ф01-002) </summary>
		[FwCultureLocalize("EPublicationDataKey_Format", typeof(Strings))]
		Format = 76,
		/// <summary> Тип расстановки </summary>
		[FwCultureLocalize("EPublicationDataKey_TypeOfArrangements", typeof(Strings))]
		ArrangementType = 77,
		/// <summary> Доп ссылки для доб карт (Доп-ные ссылки для заголовка добавочной карточки) </summary>
		[FwCultureLocalize("EPublicationDataKey_AdditionalLinksForCard", typeof(Strings))]
		AdditionalLinksForCard = 78,
		/// <summary> Тип доп ссылки </summary>
		[FwCultureLocalize("EPublicationDataKey_AdditionalLinksType", typeof(Strings))]
		AdditionalLinksType = 79,
		/// <summary> язык основного текста </summary>
		[FwCultureLocalize("EPublicationDataKey_MainContentLanguage", typeof(Strings))]
		MainContentLanguage = 80,
		/// <summary> др язык издания </summary>
		[FwCultureLocalize("EPublicationDataKey_OtherLanguage", typeof(Strings))]
		OtherLanguage = 81,
		/// <summary> Служебные отметки (Внутренние отметки, отображающиеся на левой стороне карточки ) </summary>
		[FwCultureLocalize("EPublicationDataKey_ServiceMark", typeof(Strings))]
		ServiceMark = 82,
		/// <summary> СДК </summary>
		[FwCultureLocalize("EPublicationDataKey_SDK", typeof(Strings))]
		SDK = 84,
		/// <summary> штрих код документа </summary>
		[FwCultureLocalize("EPublicationDataKey_Barcode", typeof(Strings))]
		Barcode = 85,
		/// <summary> вид содержания </summary>
		[FwCultureLocalize("EPublicationDataKey_ContentType", typeof(Strings))]
		ContentType = 86,
		/// <summary> пареллель </summary>
		[FwCultureLocalize("EPublicationDataKey_Parallel", typeof(Strings))]
		Parallel = 87,
		/// <summary> предмет </summary>
		[FwCultureLocalize("EPublicationDataKey_Subject", typeof(Strings))]
		Subject = 88,
		/// <summary> дисциплина </summary>
		[FwCultureLocalize("EPublicationDataKey_Discipline", typeof(Strings))]
		Discipline = 89,
		/// <summary> курс </summary>
		[FwCultureLocalize("EPublicationDataKey_Course", typeof(Strings))]
		Course = 90,
		/// <summary> факультет </summary>
		[FwCultureLocalize("EPublicationDataKey_Faculty", typeof(Strings))]
		Faculty = 91,
		/// <summary> специальность </summary>
		[FwCultureLocalize("EPublicationDataKey_Specialty", typeof(Strings))]
		Specialty = 92,
		/// <summary> вид публикации </summary>
		[FwCultureLocalize("EPublicationDataKey_Type", typeof(Strings))]
		Type = 93,
		/// <summary> категория </summary>
		[FwCultureLocalize("EPublicationDataKey_Category", typeof(Strings))]
		Category = 94,
		/// <summary> УДК - код </summary>
		[FwCultureLocalize("EPublicationDataKey_UDC", typeof(Strings))]
		UDC_Code = 95,
		/// <summary> ВАК - код </summary>
		[FwCultureLocalize("EPublicationDataKey_HAC", typeof(Strings))]
		HAC_Code = 97,
		/// <summary> ГРНТИ - код </summary>
		[FwCultureLocalize("EPublicationDataKey_CSCSTI", typeof(Strings))]
		CSCSTI_Code = 98,
		/// <summary> СДК - код </summary>
		[FwCultureLocalize("EPublicationDataKey_SDK", typeof(Strings))]
		SDK_Code = 99,
		/// <summary>Возрастная категория</summary>
		[FwCultureLocalize("EPublicationDataKey_AgeCategory", typeof(Strings))]
		AgeCategory = 100,