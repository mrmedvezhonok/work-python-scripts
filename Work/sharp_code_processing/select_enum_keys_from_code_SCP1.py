from Helpers import *


def main():
    source = read_fin('source_SCP1.txt')
    i = 0
    for line in source.splitlines():
        line = line.strip()
        with ofout('output_SCP1.txt') as fout:
            if not line.startswith('//') and not line.startswith('['):
                key = line.split()[0]

                minWidth, flex, maxWidth = 90, 100, 200
                # output = f'EPublicationDataKey.{key}, ({flex}, {maxWidth})'

                # print('{' + output + '},')
                output = '{EPublicationDataKey.'+key+', {}}'
                print(output)
                fout.write(output)
                fout.write('\n')
            fout.flush()


if __name__ == '__main__':
    main()
