from Helpers import *
import time


def main():
    source = read_fin('source_INSIC1.txt')
    for line in source.splitlines():
        line = line.strip()
        # with ofout('output_INSIC1.txt') as fout:
        fout = open('output_INSIC1.txt', 'w', encoding='utf-8')
        # fout = ofout('output_INSIC1.txt')
        output = line.replace('{small}', '(100, 250)')\
            .replace('{middle}', '(180, 350)')\
            .replace('{big}', '(250, 450)')\
            .replace('{large}', '(300, 500)')
        print(output + ',')
        fout.write(output)
        fout.write('\n')
        # time.sleep(0.01)
        fout.flush()
        fout.close()


if __name__ == '__main__':
    main()
