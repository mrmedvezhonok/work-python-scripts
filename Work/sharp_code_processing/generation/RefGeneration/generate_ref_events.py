def getRefEvents(ref_name, ref_translate):
  return '''

/// <summary>Добавление "{ref_translate}"</summary>
[FwCultureLocalize("ERefEvent_Create{ref_name}", typeof(Strings))]
Create{ref_name},

/// <summary>Получение "{ref_translate}"</summary>
[FwCultureLocalize("ERefEvent_Get{ref_name}", typeof(Strings))]
Get{ref_name},

/// <summary>Обновление "{ref_translate}"</summary>
[FwCultureLocalize("ERefEvent_Update{ref_name}", typeof(Strings))]
Update{ref_name},'''.replace('{ref_name}', ref_name).replace('{ref_translate}', ref_translate)


def main():
  ref_name = ''
  if not ref_name:
    ref_name = input('ref name: ')

  ref_translate = ''
  if not ref_translate:
    ref_translate = input('ref_translate: ')

  print(getRefEvents(ref_name, ref_translate))


if __name__ == '__main__':
    main()
