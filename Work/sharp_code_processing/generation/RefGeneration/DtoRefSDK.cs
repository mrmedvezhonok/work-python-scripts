
using App.Data.Proxy;
using App.Framework.Manager;
using App.Framework.Utils.Sort;
using Library.Data.Model.Ref;
using System.Reflection;

namespace Library.Web.Dto.References.Local {
	public class DtoRefSDK: AppDtoSecureBase<MRefSDK>, IAppDtoSort<MRefSDK> {
		public DtoRefSDK() { }

	public DtoRefSDK(MRefSDK obj) : base(obj?.Oid) {
		if (obj == null) return;
		Name = obj.Name;
		IsBlocked = obj.IsBlocked;
	}

	public DtoRefSDK(MRefSDK obj, bool useSecureId) : this(obj) {
		Id.UseSecureId = useSecureId;
	}

	public string Name { get; set; }

	public bool IsBlocked { get; set; }

	public FwSortObject<MRefSDK> GetSortObject(PropertyInfo property, FwSortDirection direction) {
		switch (property.Name) {
			case nameof(Name): return new FwSortObject<MRefSDK>(a => a.Name, direction);
			default: return null;
		}
	}
}
}