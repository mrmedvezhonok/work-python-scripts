using App.Data.Proxy;
using App.Framework.Manager;
using App.Framework.Utils.Sort;
using Library.Data.Model.Ref;
using System.Reflection;

namespace Library.Web.Dto.References.Local {
  public class DtoRef{ref_name} : AppDtoSecureBase<MRef{ref_name}>, IAppDtoSort<MRef{ref_name}> {
    public DtoRef{ref_name}() { }

    public DtoRef{ref_name}(MRef{ref_name} obj) : base(obj?.Oid) {
      if (obj == null) return;
      Name = obj.Name;
      Code = obj.Code;
      IsBlocked = obj.IsBlocked;
    }

    public DtoRef{ref_name}(MRef{ref_name} obj, bool useSecureId) : this(obj) {
      Id.UseSecureId = useSecureId;
    }

    public {isMultiString} Name { get; set; }
    public string Code { get; set; }

    public bool IsBlocked { get; set; }

    public FwSortObject<MRef{ref_name}> GetSortObject(PropertyInfo property, FwSortDirection direction) {
      switch (property.Name) {
        case nameof(Name): return new {nameSort}<MRef{ref_name}>(a => a.Name, direction);
        case nameof(Code): return new FwSortObject<MRef{ref_name}>(a => a.Code, direction);
        default: return null;
      }
    }
  }
}