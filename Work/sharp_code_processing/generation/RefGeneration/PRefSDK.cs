using App.Framework.Localization;
using App.Framework.Security;
using App.Security;
using Library.Common.Properties;

namespace Library.Web.Privileges.References.Local {
    [FwPrivilegeGroup("PGroupReferences", typeof(Strings))]
    [FwCultureLocalize("PRefSDK", typeof(Strings))]
    public class PRefSDK : FwPrivilegeBase {
        [FwCultureLocalize("PRefSDK_Create", typeof(Strings))]
        public AppRule Create { get; set; }

        [FwCultureLocalize("PRefSDK_Update", typeof(Strings))]
        public AppRule Update { get; set; }

        [FwCultureLocalize("PRefSDK_Block", typeof(Strings))]
        public AppRule Block { get; set; }
    }
}