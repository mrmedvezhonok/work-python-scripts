def getPRefFileContent(ref_name):
    return '''using App.Framework.Localization;
using App.Framework.Security;
using App.Security;
using Library.Common.Properties;

namespace Library.Web.Privileges.References.Local {
    [FwPrivilegeGroup("PGroupReferences", typeof(Strings))]
    [FwCultureLocalize("PRef{ref_name}", typeof(Strings))]
    public class PRef{ref_name} : FwPrivilegeBase {
        [FwCultureLocalize("PRef{ref_name}_Create", typeof(Strings))]
        public AppRule Create { get; set; }
        
        [FwCultureLocalize("PRef{ref_name}_Update", typeof(Strings))]
        public AppRule Update { get; set; }
        
        [FwCultureLocalize("PRef{ref_name}_Block", typeof(Strings))]
        public AppRule Block { get; set; }
    }
}'''.replace('{ref_name}', ref_name)


def getLocalStrings(ref_name, ref_translate):
    return [(f'{ref_name}', ref_translate), (f'PRef{ref_name}', ref_translate), (f'PRef{ref_name}_Create', 'Добавить'),
            (f'PRef{ref_name}_Update', 'Редактировать'),
            (f'PRef{ref_name}_Block', 'Блокировать/разблокировать')]


def main():
    ref_name = ''
    if not ref_name:
        ref_name = input('ref name: ')

    ref_translate = ''
    if not ref_translate:
        ref_translate = input('ref_translate: ')

    fout_name = f'PRef{ref_name}.cs'
    output = getPRefFileContent(ref_name)

    with open(fout_name, 'w', encoding='utf-8') as fout:
        fout.write(output)
        fout.flush()

    locals = getLocalStrings(ref_name)
    for local in locals:
        print(f'{local[0]}\t{local[1]}')


if __name__ == '__main__':
    main()
