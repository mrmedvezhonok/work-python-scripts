from AutoLoading.sort_strings import output
from RefGeneration.createPrivilegeFile import *
from RefGeneration.generate_ref_events import *
from RefGeneration.createDtoFile import *

from os.path import join

def make_privilegie(path, ref_name, ref_translate):
    fout_name = f'PRef{ref_name}.cs'
    output = getPRefFileContent(ref_name)

    with open(join(path, fout_name), 'w', encoding='utf-8') as fout:
        fout.write(output)
        fout.flush()

    locals = getLocalStrings(ref_name, ref_translate)
    for local in locals:
        print(f'{local[0]}\t{local[1]}')


def make_dto(path, ref_name):
    fout_name = f'DtoRef{ref_name}.cs'

    question = '''
      10 - Name
      20 - NameAndCode
      +1 - MultiString
      +2 - string
      '''
    answer = int(input(question))
    if answer == 11:
        output = getDtoNameFileContent(ref_name)
    elif answer == 12:
        output = getDtoNameFileContent(ref_name, False)
    elif answer == 21:
        output = getDtoNameAndCodeFileContent(ref_name)
    elif answer == 22:
        output = getDtoNameAndCodeFileContent(ref_name, False)
    else:
        print('Dto was not created')
        return

    with open(join(path, fout_name), 'w', encoding='utf-8') as fout:
        fout.write(output)
        fout.flush()


def make_ref_events(ref_name, ref_translate):
    fout_name = f'RefEvets_{ref_name}.text'
    output = getRefEvents(ref_name, ref_translate)
    print(output)

    with open(fout_name, 'w', encoding='utf-8') as fout:
        fout.write(output)
        fout.flush()


def generateController(path, ref_name, ref_translate):
    with open('controllerTemplate.txt', 'r', encoding='utf-8') as tfin:
        template = tfin.read()

    output = template.replace('{ref_translate}', ref_translate).replace('{ref_name}', ref_name)

    fout_name = f'Ref{ref_name}Controller.cs'

    with open(join(path, fout_name), 'w', encoding='utf-8') as fout:
        fout.write(output)
        fout.flush()


def main():
    ref_name = ''
    if not ref_name:
        ref_name = input('ref name: ')

    ref_translate = ''
    if not ref_translate:
        ref_translate = input('ref_translate: ')

    make_privilegie(r'C:\Users\Hitrin_s\source\repos\Library\Source\Library\Library.Web\Privileges\References\Local', ref_name, ref_translate)
    make_dto(r'C:\Users\Hitrin_s\source\repos\Library\Source\Library\Library.Web\Dto\References\Local', ref_name)
    make_ref_events(ref_name, ref_translate)
    generateController(r'C:\Users\Hitrin_s\source\repos\Library\Source\Library\Library.Web\Controllers\References', ref_name, ref_translate)


if __name__ == '__main__':
    main()
