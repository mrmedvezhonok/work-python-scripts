def getDtoNameAndCodeFileContent(ref_name, isMultiString=True):
    with open('dto_template_name_code.txt', 'r', encoding='utf-8') as tfin:
        output = tfin.read()
    output = output.replace('{ref_name}', ref_name)
    output = output.replace('{isMultiString}', 'AppDtoMultiString')\
        .replace('{nameSort}', 'FwMultiLangSortObject') if isMultiString else output.replace('{isMultiString}', 'string').replace('{nameSort}', 'FwSortObject')
    return output


def getDtoNameFileContent(ref_name, isMultiString=True):
    with open('dto_template_name.txt', 'r', encoding='utf-8') as tfin:
        output = tfin.read()

    output = output.replace('{ref_name}', ref_name)
    output = output.replace('{isMultiString}', 'AppDtoMultiString')\
        .replace('{nameSort}', 'FwMultiLangSortObject') if isMultiString else output.replace('{isMultiString}', 'string')\
        .replace('{nameSort}', 'FwSortObject')
    return output


def main():
    ref_name = ''
    if not ref_name:
        ref_name = input('ref name: ')

    fout_name = f'DtoRef{ref_name}.cs'

    question = '''
  10 - Name
  20 - NameAndCode
  +1 - MultiString
  +2 - string
  '''
    answer = int(input(question))
    if answer == 11:
        output = getDtoNameFileContent(ref_name)
    elif answer == 12:
        output = getDtoNameFileContent(ref_name, False)
    elif answer == 21:
        output = getDtoNameAndCodeFileContent(ref_name)
    elif answer == 22:
        output = getDtoNameAndCodeFileContent(ref_name, False)

    with open(fout_name, 'w', encoding='utf-8') as fout:
        fout.write(output)
        fout.flush()


if __name__ == '__main__':
    main()
