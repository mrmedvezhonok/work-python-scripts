import re

source = '''		/// <summary> Тип инвентаризации (наименование) </summary>
		public string InvenotryTypeName { get; set; }
		/// <summary> Тип инвентаризации (Secure GUID) </summary>
		public Guid? InvenotryType { get; set; }
		/// <summary> Название инвертаризации </summary>
		public string Name { get; set; }
		/// <summary> Организация (Secure GUID)</summary>
		public Guid? OrganizationId { get; set; }
		/// <summary> Организация Короткое название </summary>
		public string OrganizationShortName { get; set; }
		/// <summary> Дата начала </summary>
		public DateTime? StartDate { get; set; }
		/// <summary> Дата начала (строка с короткой записью даты) </summary>
		public string StartDateShort { get; set; }
		/// <summary> Дата завершения </summary>
		public DateTime? FinishDate { get; set; }
		/// <summary> Дата завершения (строка с короткой записью даты) </summary>
		public string FinishDateShort { get; set; }
		/// <summary> Статус инвентаризации </summary>
		public EInventoryStatus Status { get; set; }'''

lines = source.splitlines()
template = '''/** {Comment} **/
{Name}: { name: '{Name}', type: '{Type}' },'''

comment = '-'
if __name__ == '__main__':
    for line in lines:
        line = line.strip()
        if line.startswith('///'):
            comment = re.findall(r'<summary>\s*(.*)</summary>', line)[0]
            continue
        sp = line.split()
        # print(sp)
        type = 'string'
        if sp[1] == 'DateTime?':
            type = 'date'
        name = sp[2]
        print(template.replace('{Comment}', comment).replace('{Name}', name).replace('{Type}', type))
