source = '''<table width='100%' cellspacing='0' cellpadding='0' border='0'><tbody>
	<tr><td width='12%' valign='top'></td><td width='87%' valign='top'><p><b><span>{AuthorName}</span></b></p></td></tr>
	<tr><td width='12%' valign='top'><p><span id='S4'>{AuthorMark}</span></p></td>
	<td width='87%' valign='top'><p><span>
		{Title} : {TitleInformation} {ResponsibilityInformation} -
		{EditionInformation} {EditionResponsibilityInformation} - 
		{PlaceOfPublication} {Publisher} {PublishYear}. - 
		{Capacity} {SupportingMaterials} - ({SeriesTitle} {SeriesInformation} / 
		{SeriesResponsibilityInformation} {SeriesISSN} {SeriesVolumeNumber} {Subseries} {SubseriesISSN}.)</span></p>
	<p><span>ISBN {ISBN}</span></p>
	<p><span>{Annotation}</span></p>
	<p><span>Предметные рубрики</span></p>
	<p align='right'><span>УДК {UDC}</span></p>
	<p align='right'><span>ББК {LBC}</span></p></td></tr></tbody></table>'''


if __name__ == '__main__':
    import re
    fields = re.findall(r'{(\w*)}', source)
    # fields.remove('ResponsibilityInformation')
    print(fields)

    templ = '''var {0}Field = fields.Where(x => x.Key == EPublicationDataKey.{0}).OrderBy(x => x.Index).FirstOrDefault();
    \t\tstring {0} = "";
    \t\tif ({0}Field != null) {
    \t\t\t{0} = {0}Field.Value;
    \t\t}'''
    with open('fields.txt', 'w', encoding='utf-8') as fout:
        for field in fields:
            fout.write(templ.replace('{0}', field))
            fout.write('\n\n')
        fout.flush()

    with open('params_agregate.txt', 'w', encoding='utf-8') as fout:
        for field in fields:
            fout.write('parameters.Add("{{0}}", {0});\n'.replace('{0}', field))
            fout.flush()