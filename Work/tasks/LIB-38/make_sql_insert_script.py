import json
from tasks.out_queries import *
# from out_queries import *


def main():
    data = ''
    with open('result.json', 'r', encoding='utf-8') as jf:
        data = json.load(jf)
        data = list(filter(bool, data))

    q = "INSERT INTO dbo.Ref_HAC(Gid, IsBlocked, Code, NameL1, NameL2, NameL3, NameL4, NameL5) VALUES (NEWID(), CONVERT(bit, 'False'), N'{code}', N'{name}', N'{name}', N'{name}', NULL, NULL)"

    with open('Ref_HAC.sql', 'w', encoding='utf-8-sig') as fout:
        for row in data:
            fout.write(q.format(code=row[0], name=row[1]))
            fout.write('\n')


if __name__ == '__main__':
    main()
