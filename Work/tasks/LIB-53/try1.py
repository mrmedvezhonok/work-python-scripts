from out_queries import *


def main():

    source_fn = 'common_bbk.txt'

    with open(source_fn, 'r', encoding='utf-8') as fin:
        source_str = fin.read()
        data = get_filtered_list(source_str)

    # data_set = set(data)

    error_row_nums = []
    error_rows = []
    errors = []
    import re
    # for i, row in enumerate(data):
    #     try:
    #         # parse_result = re.search(r'^([\D\S]*)\s(\d\S*)$', row)
    #         parse_result = re.search(r'^([^\d\s]*)\s(.*)$', row)
    #         # print(parse_result.groupdict())
    #         value = parse_result.group(1)
    #         code = parse_result.group(2)
    #     except Exception as ex:
    #         print(ex)
    #         print(row)
    #         print(i)
    #         error_row_nums.append(i)
    #         error_rows.append(row)
    #         print(f'---=---=---=---=---=---=---=---=---=---=---=\n')
    #
    # print(error_row_nums)
    # print(error_rows)
    table_name = 'Ref_LBC'
    q1 = f"INSERT dbo.{table_name}(Gid, IsBlocked, Code, NameL1, NameL2, NameL3, NameL4, NameL5)"
    q2 = "VALUES (NEWID(), CONVERT(bit, 'False'), N'{code}', N'{name}', N'{name}', N'{name}', NULL, NULL)"
    q = f"{q1} {q2}"

    with open('LIB-53.sql', 'w', encoding='utf-8') as f:
        for i, row in enumerate(data):
            try:
                # parse_result = re.search(r'^([\D\S]*)\s(\d\S*)$', row)
                parse_result = re.search(r'^([^\d\s]*)\s(.*)$', row)
                # print(f'{i}. row: "{row}".')
                # print(f'group 1: "{parse_result.group(1)}", \ngroup 2: "{parse_result.group(2)}"\n---=---=---=---=---=---=---=---=---=---=---=\n')
                value = parse_result.group(1).strip()
                code = parse_result.group(2)
                print(f'name: "{value}", \ncode: "{code}"\n---=---=---=---=---=---=---=---=---=---=---=\n')

            except Exception as ex:
                errors.append(str(ex))
                error_rows.append(row)
                error_row_nums.append(i)
                print(f'---=---=---=---=---=---=---=---=---=---=---=\n')

    print(errors)
    print(error_rows)
    print(error_row_nums)


if __name__ == '__main__':
    main()
