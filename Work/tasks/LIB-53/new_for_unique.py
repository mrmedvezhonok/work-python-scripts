from out_queries import *
import json


def read_lines(fn):
    with open(fn, 'r', encoding='utf-8') as fin:
        data = json.load(fin)
    return data


def main():

    table_name = 'Ref_LBC'
    data_rus = read_lines('data/LIB-53_rus_dataPairs.json')
    data_kaz = read_lines('data/LIB-53_kaz_dataPairs.json')

    print('len rus: ', len(data_rus))
    print('len kaz: ', len(data_kaz))
    kaz = 'kaz'
    rus = 'rus'
    eng = 'eng'

    # Предварительная подготовка данных (тут же фильтры некоторые)
    data = {}
    for row in data_rus:
        code = row['code'].replace("'", "''")
        name = row['name']
        data[code] = {rus: name, eng: name}

    for row in data_kaz:
        code = row['code'].replace("'", "''")
        name = row['name']
        if code in data:
            data[code][kaz] = name
        else:
            data[code] = {kaz: name, eng: name}

    # Фильтруем и объединяем данные
    with open('test_fout.txt', 'w', encoding='utf-8') as fout:
        for key in data:
            if kaz not in data[key] and rus not in data[key]:
                fout.write(f'Тупик: not kaz and not rus \t{data[key]}\n')
            elif kaz in data[key] and rus in data[key]:
                fout.write(f'Оба есть: kaz and rus \t{data[key]}\n')

            if kaz not in data[key] and rus in data[key]:
                fout.write(f'kaz not in data[{key}]\t{data[key]}\n')
                data[key][kaz] = data[key][rus]
            if rus not in data[key] and kaz in data[key]:
                fout.write(f'rus not in data[{key}]\t{data[key]}\n')
                data[key][rus] = data[key][kaz]

            if eng not in data[key]:
                print(data[key])
                data[key][eng] = data[key][rus]
                print(data[key])
                print('\n----==========------=====--==-=-=-\n')
        fout.flush()

    lines = []

    # Переводим данные в лист, хранящий объекты вида: {code, kaz, rus, eng}
    for key in data:
        lines.append({'code': key, kaz: data[key][kaz], rus: data[key][rus], eng: data[key][eng],})

    splitter = lambda x: (x[kaz], x[rus], x[eng], x['code'])
    for line in lines:
        print('line: ', line)

    out_name_code_multistring(splitter, lines, table_name)


if __name__ == '__main__':
    main()
