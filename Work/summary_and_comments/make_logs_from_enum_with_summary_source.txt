		/// <summary>Выгрузка учебных годов</summary>
		[FwCultureLocalize("ERefEvent_GetSchoolYears", typeof(Strings))]
		GetSchoolYears = 1,

		/// <summary>Выгрузка полугодий</summary>
		[FwCultureLocalize("ERefEvent_GetHalfYears", typeof(Strings))]
		GetHalfYears = 2,

		/// <summary>Выгрузка полугодий</summary>
		[FwCultureLocalize("ERefEvent_GetPeriods", typeof(Strings))]
		GetPeriods = 3,

		/// <summary>Выгрузка стран</summary>
		[FwCultureLocalize("ERefEvent_GetCountries", typeof(Strings))]
		GetCountries = 4,

		/// <summary>Выгрузка городов</summary>
		[FwCultureLocalize("ERefEvent_GetCities", typeof(Strings))]
		GetCities = 5,

		/// <summary>Выгрузка городов</summary>
		[FwCultureLocalize("ERefEvent_GetCountryGroups", typeof(Strings))]
		GetCountryGroups = 6,

		/// <summary>Выгрузка регионов</summary>
		[FwCultureLocalize("ERefEvent_GetRegions", typeof(Strings))]
		GetRegions = 7,

		/// <summary>Выгрузка областей</summary>
		[FwCultureLocalize("ERefEvent_GetDistricts", typeof(Strings))]
		GetDistricts = 8,

		/// <summary>Выгрузка классов</summary>
		[FwCultureLocalize("ERefEvent_GetKlasses", typeof(Strings))]
		GetKlasses = 9,

		/// <summary>Выгрузка ступеней обучения</summary>
		[FwCultureLocalize("ERefEvent_GetParallelSteps", typeof(Strings))]
		GetParallelSteps = 10,

		/// <summary>Выгрузка классов</summary>
		[FwCultureLocalize("ERefEvent_GetParallels", typeof(Strings))]
		GetParallels = 11,

		/// <summary>Выгрузка литеров</summary>
		[FwCultureLocalize("ERefEvent_GetLetters", typeof(Strings))]
		GetLetters = 12,

		/// <summary>Выгрузка организаций</summary>
		[FwCultureLocalize("ERefEvent_GetOrganizations", typeof(Strings))]
		GetOrganizations = 13,

		/// <summary>Выгрузка рдугих школ (не НИШ)</summary>
		[FwCultureLocalize("ERefEvent_GetSchools", typeof(Strings))]
		GetSchools = 14,

		/// <summary>Выгрузка разделов</summary>
		[FwCultureLocalize("ERefEvent_GetSections", typeof(Strings))]
		GetSections = 15,

		/// <summary>Выгрузка дополнительных параметров раздела</summary>
		[FwCultureLocalize("ERefEvent_GetSectionConfig", typeof(Strings))]
		GetSectionConfig = 16,

		/// <summary>выгрузка предметов</summary>
		[FwCultureLocalize("ERefEvent_GetSubjects", typeof(Strings))]
		GetSubjects = 17,

		/// <summary>выгрузка предметов</summary>
		[FwCultureLocalize("ERefEvent_GetPedagogicalLevels", typeof(Strings))]
		GetPedagogicalLevels = 18,

		/// <summary>выгрузка групп предметов</summary>
		[FwCultureLocalize("ERefEvent_GetSubjectGroups", typeof(Strings))]
		GetSubjectGroups = 19,

		/// <summary>Выгрузка элективных групп предметов</summary>
		[FwCultureLocalize("ERefEvent_GetSubjectGroupElective", typeof(Strings))]
		GetSubjectGroupElective = 20,

		/// <summary>выгрузка профиля предметов</summary>
		[FwCultureLocalize("ERefEvent_GetSubjectProfiles", typeof(Strings))]
		GetSubjectProfiles = 21,

		/// <summary>выгрузка тип учебной недели</summary>
		[FwCultureLocalize("ERefEvent_GetStudyWeekType", typeof(Strings))]
		GetStudyWeekType = 22,

		/// <summary>выгрузка смены</summary>
		[FwCultureLocalize("ERefEvent_GetSession", typeof(Strings))]
		GetSession = 23,

		/// <summary>выгрузка событий календаря</summary>
		[FwCultureLocalize("ERefEvent_GetCalendarEvent", typeof(Strings))]
		GetCalendarEvent = 24,

		/// <summary>Получение объекта MAppPerson</summary>
		[FwCultureLocalize("ERefEvent_GetPerson", typeof(Strings))]
		GetPerson = 1000,
		/// <summary>Получение объекта MEmployee</summary>
		[FwCultureLocalize("ERefEvent_GetPerson", typeof(Strings))]
		GetEmployee = 1001,

		/// <summary>Получение авторов</summary>
		[FwCultureLocalize("ERefEvent_GetAuthors", typeof(Strings))]
		GetAuthors = 1002,

		/// <summary>Получение типов носителей</summary>
		[FwCultureLocalize("ERefEvent_GetMediaTypes", typeof(Strings))]
		GetMediaTypes = 1003,

		/// <summary>Получение одной записи типа носителей</summary>
		[FwCultureLocalize("ERefEvent_GetMediaType", typeof(Strings))]
		GetMediaType = 1004,

		/// <summary>Добавление типа носителей</summary>
		[FwCultureLocalize("ERefEvent_CreateMediaType", typeof(Strings))]
		CreateMediaType = 1005,

		/// <summary>Обновление типов носителей</summary>
		[FwCultureLocalize("ERefEvent_UpdateMediaType", typeof(Strings))]
		UpdateMediaType = 1006,

		/// <summary>Получение предметных рубрик</summary>
		[FwCultureLocalize("ERefEvent_GetSubjectHeadings", typeof(Strings))]
		GetSubjectHeadings = 1007,

		/// <summary>Добавление ББК</summary>
		[FwCultureLocalize("ERefEvent_CreateLBC", typeof(Strings))]
		CreateLBC = 1008,

		/// <summary>Получение ББК</summary>
		[FwCultureLocalize("ERefEvent_GetLBC", typeof(Strings))]
		GetLBC = 1009,

		/// <summary>Редактирование ББК</summary>
		[FwCultureLocalize("ERefEvent_UpdateLBC", typeof(Strings))]
		UpdateLBC = 1010,

		/// <summary>Открытие модуля "Типы расстановок"</summary>
		[FwCultureLocalize("ERefEvent_OpenRefModuleArrangementType", typeof(Strings))]
		OpenRefModuleArrangementType = 1011,

		/// <summary>Получение "Типы расстановок"</summary>
		[FwCultureLocalize("ERefEvent_GetArrangementType", typeof(Strings))]
		GetArrangementType = 1012,

		/// <summary>Обновление "Типы расстановок"</summary>
		[FwCultureLocalize("ERefEvent_UpdateArrangementType", typeof(Strings))]
		UpdateArrangementType = 1013,

		/// <summary>Добавление "Типы расстановок"</summary>
		[FwCultureLocalize("ERefEvent_CreateArrangementType", typeof(Strings))]
		CreateArrangementType = 1014,

		/// <summary>Добавление "Категории публикаций"</summary>
		[FwCultureLocalize("ERefEvent_CreatePublicationCategory", typeof(Strings))]
		CreatePublicationCategory = 1015,

		/// <summary>Получение "Категории публикаций"</summary>
		[FwCultureLocalize("ERefEvent_GetPublicationCategory", typeof(Strings))]
		GetPublicationCategory = 1016,

		/// <summary>Обновление "Категории публикаций"</summary>
		[FwCultureLocalize("ERefEvent_UpdatePublicationCategory", typeof(Strings))]
		UpdatePublicationCategory = 1017,

		/// <summary> Добавление "ВАК (высшая аттестационная комиссия)" </summary>
		[FwCultureLocalize("ERefEvent_CreateHAC", typeof(Strings))]
		CreateHAC = 1018,

		/// <summary> Получение "ВАК (высшая аттестационная комиссия)" </summary>
		[FwCultureLocalize("ERefEvent_GetHAC", typeof(Strings))]
		GetHAC = 1019,

		/// <summary> Редактирование "ВАК (высшая аттестационная комиссия)" </summary>
		[FwCultureLocalize("ERefEvent_UpdateHAC", typeof(Strings))]
		UpdateHAC = 1020,

		/// <summary> Получение "ГРНТИ - Государственный рубрикатор научно-технической информации" </summary>
		[FwCultureLocalize("ERefEvent_GetCSCSTI", typeof(Strings))]
		GetCSCSTI = 1021,

		/// <summary> Создание "ГРНТИ - Государственный рубрикатор научно-технической информации" </summary>
		[FwCultureLocalize("ERefEvent_CreateCSCSTI", typeof(Strings))]
		CreateCSCSTI = 1022,

		/// <summary> Редактирование "ГРНТИ - Государственный рубрикатор научно-технической информации" </summary>
		[FwCultureLocalize("ERefEvent_UpdateCSCSTI", typeof(Strings))]
		UpdateCSCSTI = 1023,

		/// <summary>Добавление "Автор"</summary>
		[FwCultureLocalize("ERefEvent_CreateAuthor", typeof(Strings))]
		CreateAuthor = 1024,

		/// <summary>Получение "Автор"</summary>
		[FwCultureLocalize("ERefEvent_GetAuthor", typeof(Strings))]
		GetAuthor = 1025,

		/// <summary>Обновление "Автор"</summary>
		[FwCultureLocalize("ERefEvent_UpdateAuthor", typeof(Strings))]
		UpdateAuthor = 1026,

		/// <summary>Добавление "Вид содержимого"</summary>
		[FwCultureLocalize("ERefEvent_CreateContentType", typeof(Strings))]
		CreateContentType = 1027,

		/// <summary>Получение "Вид содержимого"</summary>
		[FwCultureLocalize("ERefEvent_GetContentType", typeof(Strings))]
		GetContentType = 1028,

		/// <summary>Обновление "Вид содержимого"</summary>
		[FwCultureLocalize("ERefEvent_UpdateContentType", typeof(Strings))]
		UpdateContentType = 1029,

		/// <summary>Добавление "Причина выбытия издания"</summary>
		[FwCultureLocalize("ERefEvent_CreateDisposalReason", typeof(Strings))]
		CreateDisposalReason = 1030,

		/// <summary>Получение "Причина выбытия издания"</summary>
		[FwCultureLocalize("ERefEvent_GetDisposalReason", typeof(Strings))]
		GetDisposalReason = 1031,

		/// <summary>Обновление "Причина выбытия издания"</summary>
		[FwCultureLocalize("ERefEvent_UpdateDisposalReason", typeof(Strings))]
		UpdateDisposalReason = 1032,

		/// <summary>Добавление "Тип инвентаризации"</summary>
		[FwCultureLocalize("ERefEvent_CreateInventoryType", typeof(Strings))]
		CreateInventoryType = 1033,

		/// <summary>Получение "Тип инвентаризации"</summary>
		[FwCultureLocalize("ERefEvent_GetInventoryType", typeof(Strings))]
		GetInventoryType = 1034,

		/// <summary>Обновление "Тип инвентаризации"</summary>
		[FwCultureLocalize("ERefEvent_UpdateInventoryType", typeof(Strings))]
		UpdateInventoryType = 1035,

		/// <summary>Добавление "Ключевые слова"</summary>
		[FwCultureLocalize("ERefEvent_CreateKeyword", typeof(Strings))]
		CreateKeyword = 1036,

		/// <summary>Получение "Ключевые слова"</summary>
		[FwCultureLocalize("ERefEvent_GetKeyword", typeof(Strings))]
		GetKeyword = 1037,

		/// <summary>Обновление "Ключевые слова"</summary>
		[FwCultureLocalize("ERefEvent_UpdateKeyword", typeof(Strings))]
		UpdateKeyword = 1038,

		/// <summary>Добавление "Место публикации"</summary>
		[FwCultureLocalize("ERefEvent_CreatePlacesOfPublications", typeof(Strings))]
		CreatePlacesOfPublications = 1039,

		/// <summary>Получение "Место публикации"</summary>
		[FwCultureLocalize("ERefEvent_GetPlacesOfPublications", typeof(Strings))]
		GetPlacesOfPublications = 1040,

		/// <summary>Обновление "Место публикации"</summary>
		[FwCultureLocalize("ERefEvent_UpdatePlacesOfPublications", typeof(Strings))]
		UpdatePlacesOfPublications = 1041,

		/// <summary>Добавление "СДК"</summary>
		[FwCultureLocalize("ERefEvent_CreateSDK", typeof(Strings))]
		CreateSDK = 1042,

		/// <summary>Получение "СДК"</summary>
		[FwCultureLocalize("ERefEvent_GetSDK", typeof(Strings))]
		GetSDK = 1043,

		/// <summary>Обновление "СДК"</summary>
		[FwCultureLocalize("ERefEvent_UpdateSDK", typeof(Strings))]
		UpdateSDK = 1044,

		/// <summary>Добавление "Издательство"</summary>
		[FwCultureLocalize("ERefEvent_CreatePublishers", typeof(Strings))]
		CreatePublishers = 1045,

		/// <summary>Получение "Издательство"</summary>
		[FwCultureLocalize("ERefEvent_GetPublishers", typeof(Strings))]
		GetPublishers = 1046,

		/// <summary>Обновление "Издательство"</summary>
		[FwCultureLocalize("ERefEvent_UpdatePublishers", typeof(Strings))]
		UpdatePublishers = 1047,

		/// <summary>Добавление "Службные отметки"</summary>
		[FwCultureLocalize("ERefEvent_CreateServiceMarks", typeof(Strings))]
		CreateServiceMarks = 1048,

		/// <summary>Получение "Службные отметки"</summary>
		[FwCultureLocalize("ERefEvent_GetServiceMarks", typeof(Strings))]
		GetServiceMarks = 1049,

		/// <summary>Обновление "Службные отметки"</summary>
		[FwCultureLocalize("ERefEvent_UpdateServiceMarks", typeof(Strings))]
		UpdateServiceMarks = 1050,

		/// <summary>Добавление "Сигла библиотеки"</summary>
		[FwCultureLocalize("ERefEvent_CreateSigleOfLibrary", typeof(Strings))]
		CreateSigleOfLibrary = 1051,

		/// <summary>Получение "Сигла библиотеки"</summary>
		[FwCultureLocalize("ERefEvent_GetSigleOfLibrary", typeof(Strings))]
		GetSigleOfLibrary = 1052,

		/// <summary>Обновление "Сигла библиотеки"</summary>
		[FwCultureLocalize("ERefEvent_UpdateSigleOfLibrary", typeof(Strings))]
		UpdateSigleOfLibrary = 1053,

		/// <summary>Добавление "Сигла хранения экземпляров"</summary>
		[FwCultureLocalize("ERefEvent_CreateSigleStorageInstances", typeof(Strings))]
		CreateSigleStorageInstances = 1054,

		/// <summary>Получение "Сигла хранения экземпляров"</summary>
		[FwCultureLocalize("ERefEvent_GetSigleStorageInstances", typeof(Strings))]
		GetSigleStorageInstances = 1055,

		/// <summary>Обновление "Сигла хранения экземпляров"</summary>
		[FwCultureLocalize("ERefEvent_UpdateSigleStorageInstances", typeof(Strings))]
		UpdateSigleStorageInstances = 1056,

		/// <summary>Добавление "Поставщик"</summary>
		[FwCultureLocalize("ERefEvent_CreateSupplier", typeof(Strings))]
		CreateSupplier = 1057,

		/// <summary>Получение "Поставщик"</summary>
		[FwCultureLocalize("ERefEvent_GetSupplier", typeof(Strings))]
		GetSupplier = 1058,

		/// <summary>Обновление "Поставщик"</summary>
		[FwCultureLocalize("ERefEvent_UpdateSupplier", typeof(Strings))]
		UpdateSupplier = 1059,

		/// <summary>Добавление "УДК (Универссльная десятичная классификация)"</summary>
		[FwCultureLocalize("ERefEvent_CreateUDC", typeof(Strings))]
		CreateUDC = 1060,

		/// <summary>Получение "УДК (Универссльная десятичная классификация)"</summary>
		[FwCultureLocalize("ERefEvent_GetUDC", typeof(Strings))]
		GetUDC = 1061,

		/// <summary>Обновление "УДК (Универссльная десятичная классификация)"</summary>
		[FwCultureLocalize("ERefEvent_UpdateUDC", typeof(Strings))]
		UpdateUDC = 1062,

		/// <summary>Получение "Возрастные категории"</summary>
		[FwCultureLocalize("ERefEvent_GetAgeCategories", typeof(Strings))]
		GetAgeCategories = 1063,

		/// <summary>Добавление "Возрастные категории"</summary>
		[FwCultureLocalize("ERefEvent_CreateAgeCategory", typeof(Strings))]
		CreateAgeCategory = 1064,

		/// <summary>Получение "Возрастные категории"</summary>
		[FwCultureLocalize("ERefEvent_GetAgeCategory", typeof(Strings))]
		GetAgeCategory = 1065,

		/// <summary>Обновление "Возрастные категории"</summary>
		[FwCultureLocalize("ERefEvent_UpdateAgeCategory", typeof(Strings))]
		UpdateAgeCategory = 1066,