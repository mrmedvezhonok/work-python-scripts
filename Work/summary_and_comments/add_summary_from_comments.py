s = '''	[FwCultureLocalize("EPublicationDataKey_UDC", typeof(Strings))]
		UDC_Code = 95, // УДК
		[FwCultureLocalize("EPublicationDataKey_LBC", typeof(Strings))]
		LBC_Code = 96, // ББК
		[FwCultureLocalize("EPublicationDataKey_HAC", typeof(Strings))]
		HAC_Code = 97, // ВАК
		[FwCultureLocalize("EPublicationDataKey_CSCSTI", typeof(Strings))]
		CSCSTI_Code = 98, // ГРНТИ
		[FwCultureLocalize("EPublicationDataKey_SDK", typeof(Strings))]
		SDK_Code = 99, // СДК'''


if __name__ == '__main__':
    lines = s.splitlines()
    blocks = []
    obj = {}
    for line in lines:
        line = line.strip()
        if not line or line.startswith('//'): continue
        if line.startswith('[FwCulture'):
            obj['top'] = line
        else:
            # obj['bottom'] = line
            sp = line.split('//')
            if len(sp) > 1:
                val, comment = sp[0].strip(), sp[1].strip()
                obj['comment'] = comment
                obj['bottom'] = val
            else:
                obj['bottom'] = line
                print('no comments for: ',line)
            blocks.append(obj)
            obj = {}

    with open('commented_EPublicationData.txt', 'w', encoding='utf-8') as fout:
        for block in blocks:
            if 'comment' in block:
                fout.write(f"/// <summary> {block['comment']} </summary>\n")
            fout.write(block['top'])
            fout.write('\n')
            fout.write(block['bottom'])
            fout.write('\n')
            fout.flush()