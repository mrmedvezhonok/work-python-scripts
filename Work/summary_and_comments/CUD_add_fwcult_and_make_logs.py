import re

if __name__ == '__main__':
    template = '[FwCultureLocalize("{0}_{1}", typeof(Strings))]'

    with open('make_fwcult_and_logs_from_cud.txt', 'r', encoding='utf-8') as fin:
        source = fin.read()

    cn_regex = r'public class ([\w]*).*'
    ce_regex = r'public enum ([\w]*).*'
    op_regex = r'public AppRule ([\w]*).*'
    enum_field_regex = r'[^#\[/]\w+\s?=?\s?\d?'

    lines = source.splitlines()
    ops = {
        'Create': 'Добавить',
        'CreateSubject': 'Добавить',
        'Add': 'Добавить',
        'Update': 'Редактировать',
        'UpdateSubject': 'Редактировать',
        'Edit': 'Редактировать',
        'Delete': 'Удалить',
        'DeleteSubject': 'Удалить',
        'GenerateData': 'Генерация данных',
    }

    cn = ''
    op = ''

    locals = []

    for line in lines:
        line = line.strip()
        if not line.startswith('public'):
            is_trash_line = len(re.findall(r'[#\[\]/]+', line)) > 0
            if is_trash_line: continue
            enum_fields = re.findall(enum_field_regex, line)
            print(line)
            print(enum_fields)

            if len(enum_fields) > 0:
                op = enum_fields[0]
                print(f'// {ops.get(op)}')
                print(template.format(cn, op))
                print(line)
                locals.append(f'{cn}_{op}\t{ops[op]}')
            continue

        if line.startswith('public class'):
            cn = re.findall(cn_regex, line)[0]
            continue
        if line.startswith('public enum'):
            cn = re.findall(ce_regex, line)[0]
            continue

        op = re.findall(op_regex, line)[0]
        print(f'// {ops.get(op)}')
        print(template.format(cn, op))
        print(line)
        locals.append(f'{cn}_{op}\t{ops[op]}')

    print('-_'*15)
    print('\n'*2)

    for local in locals:
        print(local)
