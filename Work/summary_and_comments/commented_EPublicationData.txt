/// <summary> УДК </summary>
[FwCultureLocalize("EPublicationDataKey_UDC", typeof(Strings))]
UDC_Code = 95,
/// <summary> ББК </summary>
[FwCultureLocalize("EPublicationDataKey_LBC", typeof(Strings))]
LBC_Code = 96,
/// <summary> ВАК </summary>
[FwCultureLocalize("EPublicationDataKey_HAC", typeof(Strings))]
HAC_Code = 97,
/// <summary> ГРНТИ </summary>
[FwCultureLocalize("EPublicationDataKey_CSCSTI", typeof(Strings))]
CSCSTI_Code = 98,
/// <summary> СДК </summary>
[FwCultureLocalize("EPublicationDataKey_SDK", typeof(Strings))]
SDK_Code = 99,
