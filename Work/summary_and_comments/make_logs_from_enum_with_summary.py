import re

if __name__ == '__main__':

    with open('make_logs_from_enum_with_summary_source.txt', 'r', encoding='utf-8') as fin:
        source = fin.read()

    lines = list(filter(lambda x: x and x.startswith('[FwCultureLocalize') or x.startswith('/// <summary>'), list(map(lambda x: x.strip(), source.splitlines()))))

    # a = re.search(r'"(\w*)"', '[FwCultureLocalize("EReaderLogType_OpenModule", typeof(Strings))]').group(1)
    # b = re.search(r'>(.*)<', '/// <summary>Получения или фильтрация читателей</summary>').group(1)

    pairs = []
    keys = []
    vals = []
    for line in lines:
        if line.startswith('[FwCultureLocalize'):
            keys.append(re.search(r'"(\w*)"', line).group(1))
        elif line.startswith('/// <summary>'):
            vals.append(re.search(r'>(.*)<', line).group(1))

    for key, value in zip(keys, vals):
         pairs.append((key,value))
         print(f'{key}\t{value}')

    print(pairs)

