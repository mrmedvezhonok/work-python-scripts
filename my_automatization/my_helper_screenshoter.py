from pyautogui import *
import time
from datetime import datetime


def make_screenshot():
    keyDown('shift')
    press('prtscr')
    keyUp('shift')


def main():
    time.sleep(5)
    timer = 40 # secs
    longness = 20 * 60 # seconds
    current_time = 0

    while current_time <= longness:
        print('make screenshot. ', str(datetime.now().time())[:-7])
        current_time += timer
        make_screenshot()
        time.sleep(timer)


if __name__ == '__main__':
    main()
