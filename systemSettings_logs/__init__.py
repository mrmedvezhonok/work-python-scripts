def main():
    template = '''//{n}
    var {n2} = Enum.GetValues(typeof({lt})).Cast<{lt}>();
    var dict{n1} = {n2}.ToDictionary(x => $"{nameof({lt})}_{x.ToString()}", x => x.GetLocalizedAttribute() != null ? new MultiString(x.GetLocalizedAttribute()) as IFwMultiLanguageString : new MultiString(x.ToString()));
    foreach (var item in dict{n1}) {
        dictionaryAuditEvents.Add(item.Key, new MultiString($"{Strings.INSERT_HERE_STRING_NAME}: {item.Value}"));
    }\n\n'''
    library_logtypes = ['EArrivalLogType', 'EArrivalEvent', 'EBarcodeLogType', 'EBarcodeEvent',
                'EDisposalLogType', 'EdisposalEvent', 'EInstancesTakenByReaderLogType',
                'EInventoryLogType', 'EInventoryEvent',
                'EProfileLogType', 'EPublicationLogType', 'EPublicationEvent', 'EPublicationInstanceEvent',
                'EReaderLogType', 'EReaderEvent', 'ERedisCacheLogType', 'ERefControllerEvent',
                'ERefEvent', 'EReturnLogType']

    tup_log_types = ['ELogTupInfo', 'ELogTupAudit', 'ESSMLogEvent', 'ERefLogEvent']

    current_log_types = tup_log_types

    with open('out_logs.txt', 'w', encoding='utf-8') as fout:
         for lt in current_log_types:
            n = lt[1:-len('LogType')] if lt.endswith('LogType') else lt[1:-len('Event')]
            n1 = lt[1:]
            n2 = n1[:1].lower() + n1[1:]
            output = template.replace('{n}', n).replace('{n1}', n1).replace('{n2}', n2).replace('{lt}', lt)
            fout.write(output)
            fout.flush()
            print(output)


if __name__ == '__main__':
    main()
